﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FurionApiDemo.Database.Migrations.Migrations
{
    public partial class v100 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LogAudit",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TableName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ColumnName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NewValue = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    OldValue = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    UserId = table.Column<long>(type: "bigint", nullable: false),
                    UserName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Operate = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LogAudit", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LogOp",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    OpType = table.Column<int>(type: "int", nullable: true),
                    Success = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Message = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Ip = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Location = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Browser = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Os = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Url = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClassName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MethodName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ReqMethod = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Param = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Result = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ElapsedTime = table.Column<long>(type: "bigint", nullable: false),
                    OpTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    Account = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LogOp", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LogVis",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Success = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Message = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Ip = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Location = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Browser = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Os = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    VisType = table.Column<int>(type: "int", nullable: true),
                    VisTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    Account = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LogVis", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Menu",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Pid = table.Column<long>(type: "bigint", nullable: false),
                    Pids = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(32)", maxLength: 32, nullable: false),
                    Title = table.Column<string>(type: "nvarchar(32)", maxLength: 32, nullable: true),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Type = table.Column<int>(type: "int", nullable: false),
                    Icon = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Router = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Component = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Permission = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    OpenType = table.Column<int>(type: "int", nullable: false),
                    Visible = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Link = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Redirect = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Weight = table.Column<int>(type: "int", nullable: false),
                    Sort = table.Column<int>(type: "int", nullable: false),
                    IsTab = table.Column<int>(type: "int", nullable: false),
                    Remark = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false),
                    ParentId = table.Column<long>(type: "bigint", nullable: true),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Menu", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Menu_Menu_ParentId",
                        column: x => x.ParentId,
                        principalTable: "Menu",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Role",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false, comment: "名称"),
                    Status = table.Column<int>(type: "int", nullable: false, comment: "状态（1正常 2停用 3删除）"),
                    Code = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "编码"),
                    Sort = table.Column<int>(type: "int", nullable: false, comment: "排序"),
                    Remark = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "备注"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Role", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false, comment: "用户名"),
                    Password = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "密码"),
                    WXUnionId = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "微信UnionId"),
                    WXWebOpenId = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "微信公众号OpenId"),
                    WXAppOpenId = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "微信小程序OpenId"),
                    WXMobileOpenId = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "微信移动端OpenId"),
                    LastLoginIp = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true, comment: "最后登录IP"),
                    LastLoginTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false, comment: "最后登录时间"),
                    Status = table.Column<int>(type: "int", nullable: false, comment: "状态-正常_1、停用_2、删除_3"),
                    AdminType = table.Column<int>(type: "int", nullable: false, comment: "管理员类型-超级管理员_1、管理员_2、非管理员_3"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RoleMenu",
                columns: table => new
                {
                    RoleId = table.Column<long>(type: "bigint", nullable: false, comment: "角色Id"),
                    MenuId = table.Column<long>(type: "bigint", nullable: false, comment: "菜单Id")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleMenu", x => new { x.RoleId, x.MenuId });
                    table.ForeignKey(
                        name: "FK_RoleMenu_Menu_MenuId",
                        column: x => x.MenuId,
                        principalTable: "Menu",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RoleMenu_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserInfo",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<long>(type: "bigint", nullable: false, comment: "用户名ID"),
                    NickName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "昵称"),
                    AvatarUrl = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "头像"),
                    Birthday = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "生日"),
                    Sex = table.Column<int>(type: "int", nullable: false, comment: "性别-男_1、女_2"),
                    Email = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true, comment: "邮箱"),
                    Phone = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true, comment: "手机"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserInfo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserInfo_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserRole",
                columns: table => new
                {
                    UserId = table.Column<long>(type: "bigint", nullable: false, comment: "用户Id"),
                    RoleId = table.Column<long>(type: "bigint", nullable: false, comment: "角色Id")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRole", x => new { x.RoleId, x.UserId });
                    table.ForeignKey(
                        name: "FK_UserRole_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserRole_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Menu",
                columns: new[] { "Id", "Code", "Component", "CreatedTime", "CreatedUserId", "Icon", "IsDeleted", "IsTab", "Link", "Name", "OpenType", "ParentId", "Permission", "Pid", "Pids", "Redirect", "Remark", "Router", "Sort", "Status", "Title", "Type", "UpdatedTime", "UpdatedUserId", "Visible", "Weight" },
                values: new object[,]
                {
                    { 1L, "system_home", "common/home", null, null, "shouye", false, 1, null, "首页", 0, null, null, 0L, "[0],", "", null, "/home", 1, 1, "首页", 0, null, null, "Y", 1 },
                    { 33L, "system_manager_tools_swagger", "common/Iframe", null, null, "jianbao", false, 0, "http://localhost:53785/", "接口文档", 2, null, null, 32L, "[0],[32],", null, null, "/swagger", 1, 1, null, 1, null, null, "Y", 1 },
                    { 32L, "system_manager_tools", "/PageView", null, null, "SCMliucheng", false, 0, null, "开发管理", 0, null, null, 0L, "[0],", null, null, "/tools", 100, 1, null, 0, null, null, "Y", 1 },
                    { 31L, "system_manager_log_clear", "", null, null, "", false, 1, null, "系统日志清理", 0, null, "sysLog:clear", 29L, "[0],[3],[29],", "", null, "", 2, 1, "", 2, null, null, "Y", 1 },
                    { 30L, "system_manager_log_query", "", null, null, "", false, 1, null, "系统日志查询", 0, null, "sysLog:query", 29L, "[0],[3],[29],", "", null, "", 1, 1, "", 2, null, null, "Y", 1 },
                    { 29L, "system_manager_log", "modules/sys/log", null, null, "wenjian", false, 1, null, "系统日志", 0, null, null, 3L, "[0],[3],", "", null, "/system/log", 6, 1, "系统日志", 1, null, null, "Y", 1 },
                    { 28L, "system_manager_config_edit", "", null, null, "", false, 1, null, "系统配置编辑", 0, null, "sysConfig:edit", 26L, "[0],[3],[26],", "", null, "", 2, 1, "", 2, null, null, "Y", 1 },
                    { 27L, "system_manager_config_query", "", null, null, "", false, 1, null, "系统配置查询", 0, null, "sysConfig:query", 26L, "[0],[3],[26],", "", null, "", 1, 1, "", 2, null, null, "Y", 1 },
                    { 26L, "system_manager_config", "modules/sys/config", null, null, "xiuli", false, 1, null, "系统配置", 0, null, null, 3L, "[0],[3],", "", null, "/system/config", 5, 1, "系统配置", 1, null, null, "Y", 1 },
                    { 25L, "system_manager_role_menu", "", null, null, "", false, 1, null, "用户授权", 0, null, "sysUser:roleMenu", 19L, "[0],[3],[19],", "", null, "", 6, 1, "", 2, null, null, "Y", 1 },
                    { 24L, "system_manager_role_detail", "", null, null, "", false, 1, null, "角色详情", 0, null, "sysUser:detail", 19L, "[0],[3],[19],", "", null, "", 5, 1, "", 2, null, null, "Y", 1 },
                    { 23L, "system_manager_role_delete", "", null, null, "", false, 1, null, "角色删除", 0, null, "sysUser:delete", 19L, "[0],[3],[19],", "", null, "", 4, 1, "", 2, null, null, "Y", 1 },
                    { 22L, "system_manager_role_edit", "", null, null, "", false, 1, null, "角色编辑", 0, null, "sysUser:edit", 19L, "[0],[3],[19],", "", null, "", 3, 1, "", 2, null, null, "Y", 1 },
                    { 21L, "system_manager_role_add", "", null, null, "", false, 1, null, "角色新增", 0, null, "sysUser:add", 19L, "[0],[3],[19],", "", null, "", 2, 1, "", 2, null, null, "Y", 1 },
                    { 20L, "system_manager_role_query", "", null, null, "", false, 1, null, "角色查询", 0, null, "sysUser:query", 19L, "[0],[3],[19],", "", null, "", 1, 1, "", 2, null, null, "Y", 1 },
                    { 18L, "system_manager_user_role", "", null, null, "", false, 1, null, "用户授权角色", 0, null, "sysUser:userRole", 12L, "[0],[3],[12],", "", null, "", 6, 1, "", 2, null, null, "Y", 1 },
                    { 19L, "system_manager_role", "modules/sys/role", null, null, "tuandui", false, 1, null, "角色", 0, null, null, 3L, "[0],[3],", "", null, "/system/user", 4, 1, "角色", 1, null, null, "Y", 1 },
                    { 16L, "system_manager_user_delete", "", null, null, "", false, 1, null, "用户删除", 0, null, "sysUser:delete", 12L, "[0],[3],[12],", "", null, "", 4, 1, "", 2, null, null, "Y", 1 },
                    { 2L, "system_theme", "common/home", null, null, "theme", false, 1, null, "主题", 0, null, null, 0L, "[0],", "", null, "/theme", 2, 1, "主题", 0, null, null, "Y", 1 },
                    { 3L, "system_system_manager", "modules/sys", null, null, "shezhi_xitong", false, 1, null, "系统管理", 0, null, null, 0L, "[0],", "", null, "/system", 3, 1, "系统管理", 0, null, null, "Y", 1 },
                    { 4L, "system_manager_info", "modules/sys/menu", null, null, "shujuku", false, 1, null, "系统信息", 0, null, null, 3L, "[0],[3],", "", null, "/system/info", 1, 1, "系统信息", 1, null, null, "Y", 1 },
                    { 5L, "system_manager_info_query", "", null, null, "", false, 1, null, "系统信息查询", 0, null, "sysInfo:query", 4L, "[0],[3],[4],", "", null, "", 1, 1, "", 0, null, null, "Y", 1 },
                    { 6L, "system_manager_menu", "modules/sys/menu", null, null, "fenlei", false, 1, null, "菜单", 0, null, null, 3L, "[0],[3],", "", null, "/system/menu", 2, 1, "菜单", 1, null, null, "Y", 1 },
                    { 17L, "system_manager_user_detail", "", null, null, "", false, 1, null, "用户详情", 0, null, "sysUser:detail", 12L, "[0],[3],[12],", "", null, "", 5, 1, "", 2, null, null, "Y", 1 },
                    { 8L, "system_manager_menu_add", "", null, null, "", false, 1, null, "菜单新增", 0, null, "sysMenu:add", 6L, "[0],[3],[6],", "", null, "", 2, 1, "", 2, null, null, "Y", 1 },
                    { 7L, "system_manager_menu_query", "", null, null, "", false, 1, null, "菜单查询", 0, null, "sysMenu:query", 6L, "[0],[3],[6],", "", null, "", 1, 1, "", 2, null, null, "Y", 1 },
                    { 10L, "system_manager_menu_delete", "", null, null, "", false, 1, null, "菜单删除", 0, null, "sysMenu:delete", 6L, "[0],[3],[6],", "", null, "", 4, 1, "", 2, null, null, "Y", 1 },
                    { 11L, "system_manager_menu_refence", "", null, null, "", false, 1, null, "菜单刷新缓存", 0, null, "sysMenu:refence", 6L, "[0],[3],[6],", "", null, "", 5, 1, "", 2, null, null, "Y", 1 },
                    { 12L, "system_manager_user", "modules/sys/user", null, null, "kehu", false, 1, null, "用户", 0, null, null, 3L, "[0],[3]", "", null, "/system/user", 3, 1, "用户", 1, null, null, "Y", 1 },
                    { 13L, "system_manager_user_query", "", null, null, "", false, 1, null, "用户查询", 0, null, "sysUser:query", 12L, "[0],[3],[12],", "", null, "", 1, 1, "", 2, null, null, "Y", 1 },
                    { 14L, "system_manager_user_add", "", null, null, "", false, 1, null, "用户新增", 0, null, "sysUser:add", 12L, "[0],[3],[12],", "", null, "", 2, 1, "", 2, null, null, "Y", 1 },
                    { 15L, "system_manager_user_edit", "", null, null, "", false, 1, null, "用户编辑", 0, null, "sysUser:edit", 12L, "[0],[3],[12],", "", null, "", 3, 1, "", 2, null, null, "Y", 1 },
                    { 9L, "system_manager_menu_edit", "", null, null, "", false, 1, null, "菜单编辑", 0, null, "sysMenu:edit", 6L, "[0],[3],[6],", "", null, "", 3, 1, "", 2, null, null, "Y", 1 }
                });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "Code", "CreatedTime", "CreatedUserId", "IsDeleted", "Name", "Remark", "Sort", "Status", "UpdatedTime", "UpdatedUserId" },
                values: new object[,]
                {
                    { 1L, "sys_manager_role", null, null, false, "系统管理员", "系统管理员", 100, 1, null, null },
                    { 2L, "manager_role", null, null, false, "管理员", "管理员", 101, 1, null, null },
                    { 3L, "common_role", null, null, false, "普通用户", "普通用户", 102, 1, null, null }
                });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "Id", "AdminType", "CreatedTime", "CreatedUserId", "IsDeleted", "LastLoginIp", "LastLoginTime", "Password", "Status", "UpdatedTime", "UpdatedUserId", "UserName", "WXAppOpenId", "WXMobileOpenId", "WXUnionId", "WXWebOpenId" },
                values: new object[,]
                {
                    { 1L, 1, null, null, false, "", new DateTimeOffset(new DateTime(2021, 9, 17, 16, 56, 19, 555, DateTimeKind.Unspecified).AddTicks(3936), new TimeSpan(0, 8, 0, 0, 0)), "e10adc3949ba59abbe56e057f20f883e", 1, null, null, "superAdmin", "", "", "", "" },
                    { 2L, 2, null, null, false, "", new DateTimeOffset(new DateTime(2021, 9, 17, 16, 56, 19, 556, DateTimeKind.Unspecified).AddTicks(7992), new TimeSpan(0, 8, 0, 0, 0)), "e10adc3949ba59abbe56e057f20f883e", 1, null, null, "admin", "", "", "", "" }
                });

            migrationBuilder.InsertData(
                table: "UserInfo",
                columns: new[] { "Id", "AvatarUrl", "Birthday", "CreatedTime", "CreatedUserId", "Email", "IsDeleted", "NickName", "Phone", "Sex", "UpdatedTime", "UpdatedUserId", "UserId" },
                values: new object[,]
                {
                    { 1L, "", new DateTimeOffset(new DateTime(2000, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 8, 0, 0, 0)), null, null, "379894344@qq.com", false, "超级管理员", "13219041995", 1, null, null, 1L },
                    { 2L, "", new DateTimeOffset(new DateTime(2000, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 8, 0, 0, 0)), null, null, "379894344@qq.com", false, "管理员", "13219041995", 1, null, null, 2L }
                });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[,]
                {
                    { 1L, 1L },
                    { 2L, 2L }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Menu_ParentId",
                table: "Menu",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_RoleMenu_MenuId",
                table: "RoleMenu",
                column: "MenuId");

            migrationBuilder.CreateIndex(
                name: "IX_UserInfo_UserId",
                table: "UserInfo",
                column: "UserId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserRole_UserId",
                table: "UserRole",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LogAudit");

            migrationBuilder.DropTable(
                name: "LogOp");

            migrationBuilder.DropTable(
                name: "LogVis");

            migrationBuilder.DropTable(
                name: "RoleMenu");

            migrationBuilder.DropTable(
                name: "UserInfo");

            migrationBuilder.DropTable(
                name: "UserRole");

            migrationBuilder.DropTable(
                name: "Menu");

            migrationBuilder.DropTable(
                name: "Role");

            migrationBuilder.DropTable(
                name: "User");
        }
    }
}
