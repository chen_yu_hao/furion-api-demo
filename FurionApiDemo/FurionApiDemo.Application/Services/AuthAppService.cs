﻿using Furion.DynamicApiController;
using FurionApiDemo.Core;
using FurionApiDemo.Core.ApiResult;
using FurionApiDemo.Core.Implement.Auth.Dtos;
using FurionApiDemo.Core.Implementt.Auth;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurionApiDemo.Application.Services
{
    /// <summary>
    /// 用户验证接口
    /// </summary>
    [Route("auth")]
    public class AuthAppService : IDynamicApiController
    {
        private readonly IAuthService _authService;

        private readonly ILogger<AuthAppService> _logger;

        public AuthAppService(IAuthService authService,
            ILogger<AuthAppService> logger)
        {
            _authService = authService;
            _logger = logger;
        }

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("Login")]
        public async Task<ApiResult<LoginOutput>> LoginAsync([Required] LoginInput input)
        {
            var result = new ApiResult<LoginOutput>();
            try
            {
                result.Data = await _authService.LoginAsync(input);
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 登出
        /// </summary>
        /// <returns></returns>
        [HttpPost("Logout")]
        public async Task<ApiResultBase> LogoutAsync()
        {
            var result = new ApiResultBase();
            try
            {
                await _authService.LogoutAsync();
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 验证码
        /// </summary>
        /// <returns></returns>
        [HttpPost("GetCaptcha")]
        public async Task<ApiResult<CaptchaOutput>> GetCaptcha()
        {
            var result = new ApiResult<CaptchaOutput>();
            try
            {
                result.Data = await _authService.GetCaptcha();
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 设置登录信息
        /// </summary>
        /// <returns></returns>
        [HttpPost("SetLoginInfo")]
        public async Task SetLoginInfo()
        {
            try
            {
                await _authService.SetLoginInfo();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }
    }
}
