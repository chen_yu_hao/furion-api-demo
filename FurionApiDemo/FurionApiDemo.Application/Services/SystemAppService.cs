﻿using Furion;
using Furion.Authorization;
using Furion.DataEncryption;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using FurionApiDemo.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using FurionApiDemo.Util.Helper;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using FurionApiDemo.Core.Implement.System.Dto;
using FurionApiDemo.Core.ApiResult;
using Microsoft.Extensions.Logging;

namespace FurionApiDemo.Application.Services
{
    /// <summary>
    /// 系统服务接口
    /// </summary>
    [Route("system")]
    public class SystemAppService : IDynamicApiController
    {
        private readonly ISystemService _systemService;

        private readonly ILogger<SystemAppService> _logger;

        public SystemAppService(ISystemService systemService, 
            ILogger<SystemAppService> logger)
        {
            _systemService = systemService;
            _logger = logger;
        }
        /// <summary>
        /// 获取网络信息
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("GetNetInfo")]
        public ApiResult<SysNetOutput> GetNetInfo()
        {
            var result = new ApiResult<SysNetOutput>();
            try
            {
                result.Data = _systemService.GetNetInfo();
            }
            catch (System.Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }
        /// <summary>
        /// 获取服务器信息
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("GetComputerInfo")]
        public ApiResult<ComputerOutput> GetComputerInfo()
        {
            var result = new ApiResult<ComputerOutput>();
            try
            {
                result.Data = _systemService.GetComputerInfo();
            }
            catch (System.Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }
    }
}
