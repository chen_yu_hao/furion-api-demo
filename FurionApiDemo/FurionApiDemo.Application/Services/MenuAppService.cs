﻿using Furion;
using Furion.DynamicApiController;
using FurionApiDemo.Core;
using FurionApiDemo.Core.ApiResult;
using FurionApiDemo.Core.Implement.Menu;
using FurionApiDemo.Core.Implement.Menu.Dto;
using FurionApiDemo.Core.Manager;
using FurionApiDemo.Core.Manager.User;
using FurionApiDemo.Core.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurionApiDemo.Application.Services
{
    /// <summary>
    /// 菜单接口
    /// </summary>
    [Route("Menu")]
    public class MenuAppService : IDynamicApiController
    {
        private readonly IMenuService _menuService;

        private readonly ILogger<MenuAppService> _logger;

        public MenuAppService(IMenuService menuService, ILogger<MenuAppService> logger)
        {
            _menuService = menuService;
            _logger = logger;
        }

        /// <summary>
        /// 获取登录人员的菜单权限
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("LoginPermissionList")]
        public async Task<ApiResult<List<string>>> GetLoginPermissionList()
        {
            var result = new ApiResult<List<string>>();
            try
            {
                result.Data = await _menuService.GetLoginPermissionList();
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 获取登录人员菜单Tree
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("GetLoginMenus")]
        public async Task<ApiResult<List<MenuOutput>>> GetLoginMenus()
        {
            var result = new ApiResult<List<MenuOutput>>();
            try
            {
                result.Data = await _menuService.GetLoginMenus();
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 获取菜单Tree
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("GetMenuList")]
        public async Task<ApiResult<List<MenuOutput>>> GetMenuList([FromQuery] MenuInput input)
        {
            var result = new ApiResult<List<MenuOutput>>();
            try
            {
                result.Data = await _menuService.GetMenuList(input);
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 获取菜单Tree
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("GetMenus")]
        public async Task<ApiResult<List<MenuOutput>>> GetMenus([FromQuery] MenuInput input)
        {
            var result = new ApiResult<List<MenuOutput>>();
            try
            {
                result.Data = await _menuService.GetMenus(input);
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 获取登录人员菜单列表
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("GetLoginMenuList")]
        public async Task<ApiResult<List<MenuOutput>>> GetLoginMenuList()
        {
            var result = new ApiResult<List<MenuOutput>>();
            try
            {
                result.Data = await _menuService.GetLoginMenuList();
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 获取路由
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("GetLoginRoutes")]
        public async Task<ApiResult<List<RouterOutput>>> GetLoginRoutes()
        {
            var result = new ApiResult<List<RouterOutput>>();
            try
            {
                result.Data = await _menuService.GetLoginRoutes();
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 刷新菜单缓存
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("RefenceMenuCache")]
        public async Task<ApiResult<bool>> RefenceMenuCache()
        {
            var result = new ApiResult<bool>();
            try
            {
                result.Data = await _menuService.RefenceMenuCache();
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 保存菜单
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("SaveMenu")]
        public async Task<ApiResult<SaveMenuOutput>> SaveMenu(SaveMenuInput input)
        {
            var result = new ApiResult<SaveMenuOutput>();
            try
            {
                result.Data = await _menuService.SaveMenu(input);
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 获取菜单信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("GetMenuById")]
        public async Task<ApiResult<SaveMenuOutput>> GetMenuById(long id)
        {
            var result = new ApiResult<SaveMenuOutput>();
            try
            {
                result.Data = await _menuService.GetMenuById(id);
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 删除菜单
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpDelete("DeleteMenu")]
        public async Task<ApiResult<bool>> DeleteMenu([FromQuery] long id)
        {
            var result = new ApiResult<bool>();
            try
            {
                if (id == 0)
                {
                    result.Data = false;
                    result.Code = ResultCode.ERRO;
                    result.Message = "请输入要删除的菜单ID";
                }
                else
                {
                    await _menuService.DeleteMenu(id);
                    result.Data = true;
                }
            }
            catch (Exception ex)
            {
                result.Data = false;
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }
    }
}
