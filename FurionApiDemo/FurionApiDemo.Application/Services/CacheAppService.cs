﻿using Furion.DynamicApiController;
using FurionApiDemo.Core;
using FurionApiDemo.Core.ApiResult;
using FurionApiDemo.Core.Implement;
using FurionApiDemo.Core.Implement.Menu.Dto;
using FurionApiDemo.Core.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurionApiDemo.Application.Services
{
    [Route("Cache")]
    public class CacheAppService : IDynamicApiController
    {
        private readonly ICacheService _cacheService;

        private readonly ILogger<CacheAppService> _logger;
        public CacheAppService(ICacheService cacheService,
            ILogger<CacheAppService> logger)
        {
            _cacheService = cacheService;
            _logger = logger;
        }
        /// <summary>
        /// 获取菜单缓存
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="appCode"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("GetMenu")]
        public async Task<ApiResult<List<MenuOutput>>> GetMenu(long userId, string appCode)
        {
            var result = new ApiResult<List<MenuOutput>>();
            try
            {
                result.Data = await _cacheService.GetMenu(userId, appCode);
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 缓存菜单
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="appCode"></param>
        /// <param name="menus"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("SetMenu")]
        public async Task<ApiResultBase> SetMenu(long userId, string appCode, List<MenuOutput> menus)
        {
            var result = new ApiResult<ApiResultBase>();
            try
            {
                await _cacheService.SetMenu(userId, appCode, menus);
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 获取权限缓存（按钮）
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet("GetPermission")]
        public async Task<ApiResult<List<string>>> GetPermission(long userId)
        {
            var result = new ApiResult<List<string>>();
            try
            {
                result.Data = await _cacheService.GetPermission(userId);
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 缓存权限
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="permissions"></param>
        /// <returns></returns>
        [HttpPost("SetPermission")]
        public async Task<ApiResultBase> SetPermission(long userId, List<string> permissions)
        {
            var result = new ApiResult<ApiResultBase>();
            try
            {
                await _cacheService.SetPermission(userId, permissions);
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 获取所有缓存关键字
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetAllCacheKeys")]
        public ApiResult<List<string>> GetAllCacheKeys()
        {
            var result = new ApiResult<List<string>>();
            try
            {
                result.Data = _cacheService.GetAllCacheKeys();
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 删除指定关键字缓存
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpDelete("DelAsync")]
        public async Task<ApiResult<bool>> DelAsync(string key)
        {
            var result = new ApiResult<bool>();
            try
            {
                result.Data = await _cacheService.DelAsync(key);
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 设置缓存
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpPost("SetAsync")]
        public async Task<ApiResult<bool>> SetAsync(string key, object value)
        {
            var result = new ApiResult<bool>();
            try
            {
                result.Data = await _cacheService.SetAsync(key, value);
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 获取缓存
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpGet("GetAsync")]
        public async Task<ApiResult<string>> GetAsync(string key)
        {
            var result = new ApiResult<string>();
            try
            {
                result.Data = await _cacheService.GetAsync(key);
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 获取缓存
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpGet("GetAsyncT")]
        public async Task<ApiResult<T>> GetAsync<T>(string key)
        {
            var result = new ApiResult<T>();
            try
            {
                result.Data = await _cacheService.GetAsync<T>(key);
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }
    }
}
