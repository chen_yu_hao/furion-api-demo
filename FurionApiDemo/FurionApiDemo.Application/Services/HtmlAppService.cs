﻿using Furion.DynamicApiController;
using FurionApiDemo.Core;
using FurionApiDemo.Core.ApiResult;
using FurionApiDemo.Core.Implement.html;
using FurionApiDemo.Core.Implement.html.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurionApiDemo.Application.Services
{
    /// <summary>
    /// 
    /// </summary>
    [Route("html")]
    public class HtmlAppService : IDynamicApiController
    {
        private readonly IHtmlService _htmlService;

        private readonly ILogger<HtmlAppService> _logger;

        public HtmlAppService(IHtmlService htmlService, ILogger<HtmlAppService> logger)
        {
            _htmlService = htmlService;
            _logger = logger;
        }
        [HttpGet("GetItems")]
        public async Task<ApiResult<List<ItemsOutput>>> GetItemsAsync()
        {
            var result = new ApiResult<List<ItemsOutput>>();
            try
            {
                result.Data = await _htmlService.GetItemsAsync();
                
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }
        [Authorize]
        [HttpPost("AddItem")]
        public async Task<ApiResult<bool>> AddItemAsync(AddItemInput input)
        {
            var result = new ApiResult<bool>();
            try
            {
                result.Data = await _htmlService.AddItemAsync(input);
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }

        [Authorize]
        [HttpPost("DeleteItem")]
        public async Task<ApiResult<bool>> DeleteItemAsync(DeleteItemInput input)
        {
            var result = new ApiResult<bool>();
            try
            {
                result.Data = await _htmlService.DeleteItemAsync(input);
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }

        [Authorize]
        [HttpPost("UpdateItem")]
        public async Task<ApiResult<bool>> UpdateItemAsync(UpdateItemInput input)
        {
            var result = new ApiResult<bool>();
            try
            {
                result.Data = await _htmlService.UpdateItemAsync(input);
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }
    }
}
