﻿using Furion;
using Furion.DynamicApiController;
using FurionApiDemo.Core;
using FurionApiDemo.Core.ApiResult;
using FurionApiDemo.Core.Entity;
using FurionApiDemo.Core.Implement;
using FurionApiDemo.Core.Implement.Menu;
using FurionApiDemo.Core.Implement.Role.Dto;
using FurionApiDemo.Core.Manager;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurionApiDemo.Application.Services
{
    /// <summary>
    /// 角色接口
    /// </summary>
    [Route("Role")]
    public class RoleAppService : IDynamicApiController
    {
        private readonly IRoleService _RoleService;

        private readonly ILogger<RoleAppService> _logger;
        public RoleAppService(IRoleService roleService,
            ILogger<RoleAppService> logger)
        {
            _RoleService = roleService;
            _logger = logger;
        }

        /// <summary>
        /// 获取角色列表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("GetRoleList")]
        public async Task<ApiResult<PagedList<RoleListOutput>>> GetRoleList([FromQuery] RolePageInput input)
        {
            var result = new ApiResult<PagedList<RoleListOutput>>();
            try
            {
                result.Data = await _RoleService.GetRoleList(input);
            }
            catch (System.Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 角色下拉（用于授权角色时选择）
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("GetRoleDropDown")]
        public async Task<ApiResult<dynamic>> GetRoleDropDown()
        {
            var result = new ApiResult<dynamic>();
            try
            {
                result.Data = await _RoleService.GetRoleDropDown();
            }
            catch (System.Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [Authorize]
        [HttpDelete("DeleteRole")]
        public async Task<ApiResultBase> DeleteRole(List<long> ids)
        {
            var result = new ApiResult<ApiResultBase>();
            try
            {
                await _RoleService.DeleteRole(ids);
            }
            catch (System.Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 获取角色
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("GetRoleInfo")]
        public async Task<ApiResult<RoleOutput>> GetRoleInfo([Required] long id)
        {
            var result = new ApiResult<RoleOutput>();
            try
            {
                result.Data = await _RoleService.GetRoleInfo(id);
            }
            catch (System.Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 授权角色菜单
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPut("GrantMenu")]
        public async Task<ApiResultBase> GrantMenu(GrantRoleMenuInput input)
        {
            var result = new ApiResult<SysRole>();
            try
            {
                await _RoleService.GrantMenu(input);
            }
            catch (System.Exception ex)
            {
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 保存用户
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("SaveRole")]
        public async Task<ApiResult<bool>> SaveRole(SaveRoleInput input)
        {
            var result = new ApiResult<bool>();
            try
            {
                await _RoleService.SaveRole(input);
                result.Data = true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                result.Data = false;
            }
            return result;
        }
    }
}
