﻿using Furion.DependencyInjection;
using Furion.DynamicApiController;
using FurionApiDemo.Core;
using FurionApiDemo.Core.ApiResult;
using FurionApiDemo.Core.Entity;
using FurionApiDemo.Core.Implement.User.Dtos;
using FurionApiDemo.Core.Implementt.User.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurionApiDemo.Application.Services
{
    /// <summary>
    /// 用户服务接口
    /// </summary>
    [Route("user")]
    public class UserAppService : IDynamicApiController
    {
        private readonly IUserService _userService;

        private readonly ILogger<UserAppService> _logger;
        public UserAppService(IUserService userService,
            ILogger<UserAppService> logger)
        {
            _userService = userService;
            _logger = logger;
        }

        /// <summary>
        /// 获取当前用户信息
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("GetUser")]
        public async Task<ApiResult<UserOutput>> GetUserAsync()
        {
            var result = new ApiResult<UserOutput>();
            try
            {
                result.Data = await _userService.GetUser();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = ResultCode.ERRO;
            }
            return result;
        }


        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="id">用户ID</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("GetUserById")]
        public async Task<ApiResult<UserOutput>> GetUserAsync(long id)
        {
            var result = new ApiResult<UserOutput>();
            try
            {
                result.Data = await _userService.GetUser(id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
            }
            return result;
        }


        /// <summary>
        /// 获取用户列表信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("GetUserList")]
        public async Task<ApiResult<PagedList<UserListOutput>>> GetUserList([FromQuery] UserListInput input)
        {
            var result = new ApiResult<PagedList<UserListOutput>>();
            try
            {
                result.Data = await _userService.GetUserList(input);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = ResultCode.ERRO;
            }
            return result;
        }

        /// <summary>
        /// 保存用户信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("SaveUser")]
        public async Task<ApiResult<bool>> SaveUser(SaveUserInput input)
        {
            var result = new ApiResult<bool>();
            try
            {
                await _userService.SaveUser(input);
                result.Data = true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                result.Data = false;
            }
            return result;
        }

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [Authorize]
        [HttpDelete("DeleteUser")]
        public async Task<ApiResult<bool>> DeleteUser(List<long> ids)
        {
            var result = new ApiResult<bool>();
            try
            {
                if (ids.Count == 0)
                {
                    result.Data = false;
                    result.Code = ResultCode.ERRO;
                    result.Message = "请输入要删除的用户ID";
                }
                else
                {
                    await _userService.DeleteUser(ids);
                    result.Data = true;
                }
            }
            catch (Exception ex)
            {
                result.Data = false;
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                _logger.LogError(ex.Message);
            }
            return result;
        }
        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="oldPassword"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPut("UpdataPassword")]
        public async Task<ApiResult<bool>> UpdataPassword(string oldPassword, string newPassword)
        {
            var result = new ApiResult<bool>();
            try
            {
                await _userService.UpdataPassword(oldPassword, newPassword);
                result.Data = true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                result.Data = false;
            }
            return result;
        }
        /// <summary>
        /// 重置密码
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPut("ResetPassword")]
        public async Task<ApiResult<bool>> ResetPassword(long id)
        {
            var result = new ApiResult<bool>();
            try
            {
                await _userService.ResetPassword(id);
                result.Data = true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = ResultCode.ERRO;
                result.Message = ex.Message;
                result.Data = false;
            }
            return result;
        }
    }
}
