﻿using Microsoft.Win32;
using System;
using System.Management;//(添加对 System.Management.dll 的引用才能使用 System.Management 命名空间)
namespace FurionApiDemo.Util.Helper
{
    /// <summary>
    /// 收集机器硬件信息的相关代码片断（cpu频率、磁盘可用空间、内存容量……）
    /// </summary>
    public static class ComputerManagementHelper
    {
        #region 获取CPU频率
        /**************************************************
        * 函数名称:GetCPUFrequency()
        * 功能说明:获取CPU频率
        * 参 数:
        * 使用示列:
        * Response.Write(EC.CpuInfoObject.GetCPUFrequency());
        ************************************************/
        /// <summary>
        /// 获取CPU频率
        /// </summary>
        /// <returns>整型cpu频率</returns>
        public static int GetCPUFrequency()
        {
            RegistryKey rk = Registry.LocalMachine.OpenSubKey(@"HARDWARE\DESCRIPTION\System\CentralProcessor\0");
            object obj = rk.GetValue("~MHz");
            int CPUFrequency = (int)obj;
            return CPUFrequency;
        }
        #endregion
        #region 获取CPU名称
        /**************************************************
        * 函数名称:GetCPUName()
        * 功能说明:获取CPU名称
        * 参 数:
        * 使用示列:
        * Response.Write(EC.CpuInfoObject.GetCPUName());
        ************************************************/
        /// <summary>
        /// 获取CPU名称
        /// </summary>
        /// <returns>字符串型cpu名称</returns>
        public static string GetCPUName()
        {
            RegistryKey rk = Registry.LocalMachine.OpenSubKey(@"HARDWARE\DESCRIPTION\System\CentralProcessor\0");
            object obj = rk.GetValue("ProcessorNameString");
            string CPUName = (string)obj;
            return CPUName.TrimStart();
        }
        #endregion
        #region 磁盘空间
        /**************************************************
        * 函数名称:GetFreeDiskSpace(string DiskName)
        * 功能说明:获取磁盘空间
        * 参 数:DiskName:磁盘名称 D:或E:
        * 使用示列:
        * Response.Write(EC.CpuInfoObject.GetFreeDiskSpace("D:"));
        ************************************************/
        /// <summary>
        /// 磁盘空间
        /// </summary>
        /// <param name="DiskName">硬盘名称:D:或E:</param>
        /// <returns>整型</returns>
        public static long GetFreeDiskSpace(string DiskName)
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher();
            searcher.Query = new ObjectQuery("Select * From Win32_LogicalDisk");
            string NetCardMACAddress = "";
            foreach (ManagementObject mo in searcher.Get())
            {
                NetCardMACAddress = mo["MACAddress"].ToString().Trim();
                break;
            }
            return 1;
            //ManagementObject disk = new ManagementObject("win32_logicaldisk.deviceid=\"" + DiskName + "\"");
            //disk.Get();
            //string totalByte = disk["FreeSpace"].ToString();
            //long freeDiskSpaceMb = Convert.ToInt64(totalByte) / 1024 / 1024;
            //return freeDiskSpaceMb;
        }
        #endregion
        #region 获取操作系统版本
        /**************************************************
        * 函数名称:GetOSName()
        * 功能说明:获取获取操作系统版本名称
        * 参 数:
        * 使用示列:
        * Response.Write(EC.CpuInfoObject.GetOSName());
        ************************************************/
        /// <summary>
        /// 获取操作系统版本
        /// </summary>
        /// <returns>操作系统版本</returns>
        public static string GetOSName()
        {
            string Rev = "";
            System.OperatingSystem osInfo = System.Environment.OSVersion;
            switch (osInfo.Platform)
            {
                //Platform is Windows 95, Windows 98,Windows 98 Second Edition, or Windows Me.
                case System.PlatformID.Win32Windows:
                    switch (osInfo.Version.Major)
                    {
                        case 0:
                            Rev = "Windows 95";
                            break;
                        case 10:
                            if (osInfo.Version.Revision.ToString() == "2222A")
                                Rev = "Windows 98 Second Edition";
                            else
                                Rev = "Windows 98";
                            break;
                        case 90:
                            Rev = "Windows Me";
                            break;
                    }
                    break;
                //Platform is Windows NT 3.51, Windows NT 4.0, Windows 2000,or Windows XP.
                case System.PlatformID.Win32NT:
                    switch (osInfo.Version.Major)
                    {
                        case 3:
                            Rev = "Windows NT 3.51";
                            break;
                        case 4:
                            Rev = "Windows NT 4.0";
                            break;
                        case 5:
                            if (osInfo.Version.Minor == 0)
                                Rev = "Windows 2000";
                            else if (osInfo.Version.Minor == 2)
                                Rev = "Windows 2003";
                            else
                                Rev = "Windows XP";
                            break;
                        case 10:
                                Rev = "Windows 10";
                            break;
                    }
                    break;
            }
            return Rev;
        }
        #endregion
    }
}