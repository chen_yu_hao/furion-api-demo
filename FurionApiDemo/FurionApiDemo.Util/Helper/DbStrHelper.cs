﻿using Furion;
using Furion.DatabaseAccessor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurionApiDemo.Util.Helper
{
    public class DbStrHelper
    {
        public static string DbConnectionString = App.Configuration["ConnectionStrings:DBConnectionString"];
    }
}