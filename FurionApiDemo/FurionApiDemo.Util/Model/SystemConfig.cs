﻿using Furion.ConfigurableOptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurionApiDemo.Util.Model
{
    public class SystemConfig : IConfigurableOptions
    {
        #region 配置字段

        /// <summary>
        /// 用户默认密码
        /// </summary>
        public string DefaultUserPWD { set; get; }

        /// <summary>
        /// 背景图片资源请求url
        /// </summary>
        public string[] BackgroundGetUrl { set; get; }

        public string PageFolder { get; set; }

        public string IgnoreToken { get; set; }

        public string LogAllApi { get; set; }

        public int SnowFlakeWorkerId { get; set; } = 1;

        /// <summary>
        /// Redis链接串
        /// </summary>
        public string RedisConnectionString { get; set; }

        /// <summary>
        /// 缓存使用类型
        /// </summary>
        public string CacheType { get; set; }

        /// <summary>
        /// 是否启用MQTT服务
        /// </summary>
        public bool MqttIsOpen { set; get; } = false;

        /// <summary>
        /// MQTT服务地址
        /// </summary>
        public string MqttTcpServer { set; get; }

        /// <summary>
        /// MQTT服务端口
        /// </summary>
        public int? MqttTcpHost { get; set; } = 80;

        /// <summary>
        /// MQTT用户名
        /// </summary>
        public string MqttUserName { set; get; }

        /// <summary>
        /// MQTT密码
        /// </summary>
        public string MqttPasswrod { set; get; }

        #endregion

        #region 判断参数

        /// <summary>
        /// 缓存服务
        /// </summary>
        public string CacheService
        {
            get
            {
                if (CacheType.ToUpper() == "REDIS")
                    return "RedisCache";
                else
                    return "MemoryCache";
            }
        }

        #endregion
    }
}