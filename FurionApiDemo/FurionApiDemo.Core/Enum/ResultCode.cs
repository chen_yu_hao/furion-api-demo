﻿using System.ComponentModel;

namespace FurionApiDemo.Core
{
    /// <summary>
    /// 性别
    /// </summary>
    public enum ResultCode
    {
        /// <summary>
        /// 正常
        /// </summary>
        [Description("正常")]
        NORMAL = 200,

        /// <summary>
        /// 错误
        /// </summary>
        [Description("错误")]
        ERRO = 500
    }
}
