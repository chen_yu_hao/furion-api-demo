﻿using System.ComponentModel;

namespace FurionApiDemo.Core
{
    /// <summary>
    /// 公共状态
    /// </summary>
    public enum CommonStatus
    {
        /// <summary>
        /// 正常
        /// </summary>
        [Description("正常")]
        ENABLE = 1,

        /// <summary>
        /// 停用
        /// </summary>
        [Description("停用")]
        DISABLE = 2,

        /// <summary>
        /// 删除
        /// </summary>
        [Description("删除")]
        DELETED = 3
    }
}
