﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurionApiDemo.Core.Enum
{
    public enum CacheConType
    {
        /// <summary>
        /// 全部
        /// </summary>
        All = 0,
        /// <summary>
        /// 菜单
        /// </summary>
        Menu = 1,
        /// <summary>
        /// 权限
        /// </summary>
        Permission = 2,
        /// <summary>
        /// 角色
        /// </summary>
        Role = 3,
        /// <summary>
        /// 系统配置
        /// </summary>
        Config = 4
    }
}
