﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurionApiDemo.Core.ApiResult
{
    public class ApiResult<T>: ApiResultBase
    {
        public ApiResult()
        {
            Code = ResultCode.NORMAL;
            Message = "";
            Data = System.Activator.CreateInstance<T>();
        }

        public T Data { get; set; }
    }
    public class ApiResultBase
    {
        public ApiResultBase()
        {
            Code = ResultCode.NORMAL;
            Message = "";
        }

        public ResultCode Code { get; set; }
        public string Message { get; set; }
    }
}
