﻿using Furion.TaskScheduler;
using FurionApiDemo.Core.Implement.System.Dto;
using FurionApiDemo.Core.SignalRHub;
using FurionApiDemo.Util.Helper;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurionApiDemo.Core.SpareTimeWork
{
    /// <summary>
    /// 定时任务/后台任务
    /// </summary>
    public class SpareTimeWorker : ISpareTimeWorker
    {
        ///// <summary>
        ///// 3s 后执行
        ///// </summary>
        ///// <param name="timer"></param>
        ///// <param name="count"></param>
        //[SpareTime(3000, "jobName", DoOnce = true, StartNow = true)]
        //public void DoSomething1(SpareTimer timer, long count)
        //{
        //    Console.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        //}
        ///// <summary>
        ///// 每隔 3s 执行
        ///// </summary>
        ///// <param name="timer"></param>
        ///// <param name="count"></param>
        //[SpareTime(3000, "jobName1", StartNow = true)]
        //public void DoSomething2(SpareTimer timer, long count)
        //{
        //    Console.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        //    Console.WriteLine($"一共执行了：{count} 次");
        //}
    }
}
