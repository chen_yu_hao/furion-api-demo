﻿using FurionApiDemo.Core.ApiResult;
using FurionApiDemo.Core.Enum;
using FurionApiDemo.Core.Implement.Menu.Dto;
using FurionApiDemo.Core.Service;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FurionApiDemo.Core.Implement
{
    public interface ICacheService
    {
        Task<bool> RefenceCache(long userId, AppType appCode, CacheConType cacheType = CacheConType.All);
        Task<bool> DelAsync(string key);
        List<string> GetAllCacheKeys();
        Task<List<MenuOutput>> GetMenu(long userId, string appCode);
        Task<List<string>> GetPermission(long userId);
        Task SetMenu(long userId, string appCode, List<MenuOutput> menus);
        Task SetPermission(long userId, List<string> permissions);
        Task<bool> SetAsync(string key, object value);
        Task<string> GetAsync(string key);
        Task<T> GetAsync<T>(string key);
    }
}