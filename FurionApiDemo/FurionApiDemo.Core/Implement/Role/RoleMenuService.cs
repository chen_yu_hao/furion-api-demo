﻿using Furion.DatabaseAccessor;
using Furion.DatabaseAccessor.Extensions;
using Furion.DependencyInjection;
using FurionApiDemo.Core.Entity;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FurionApiDemo.Core.Implement
{
    /// <summary>
    /// 角色菜单
    /// </summary>
    public class RoleMenuService : IRoleMenuService, ITransient
    {
        private readonly IRepository<SysRoleMenu> _sysRoleMenuRep;  // 角色菜单表仓储   
        private readonly ICacheService _sysCacheService;

        public RoleMenuService(IRepository<SysRoleMenu> sysRoleMenuRep, ICacheService sysCacheService)
        {
            _sysRoleMenuRep = sysRoleMenuRep;
            _sysCacheService = sysCacheService;
        }

        /// <summary>
        /// 获取角色的菜单Id集合
        /// </summary>
        /// <param name="roleIdList"></param>
        /// <returns></returns>
        public async Task<List<long>> GetRoleMenuIdList(List<long> roleIdList)
        {
            return await _sysRoleMenuRep.DetachedEntities
                                        .Where(u => roleIdList.Contains(u.RoleId))
                                        .Select(u => u.MenuId).ToListAsync();
        }

        /// <summary>
        /// 授权角色菜单
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task GrantMenu(GrantRoleMenuInput input)
        {
            var roleMenus = await _sysRoleMenuRep.DetachedEntities.Where(u => u.RoleId == input.Id).ToListAsync();
            roleMenus.ForEach(u =>
            {
                u.DeleteNow();
            });

            input.GrantMenuIdList.ForEach(u =>
            {
                new SysRoleMenu
                {
                    RoleId = input.Id,
                    MenuId = u
                }.InsertNow();
            });
            await _sysCacheService.SetAsync("IsChangeRoleMenu",true);
        }

        /// <summary>
        /// 根据菜单Id集合删除对应的角色-菜单表信息
        /// </summary>
        /// <param name="menuIdList"></param>
        /// <returns></returns>
        public async Task DeleteRoleMenuListByMenuIdList(List<long> menuIdList)
        {
            var roleMenus = await _sysRoleMenuRep.DetachedEntities.Where(u => menuIdList.Contains(u.MenuId)).ToListAsync();
            roleMenus.ForEach(u =>
            {
                u.DeleteNow();
            });
            await _sysCacheService.SetAsync("IsChangeRoleMenu", true);
        }

        /// <summary>
        /// 根据角色Id删除对应的角色-菜单表关联信息
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public async Task DeleteRoleMenuListByRoleId(long roleId)
        {
            var roleMenus = await _sysRoleMenuRep.DetachedEntities.Where(u => u.RoleId == roleId).ToListAsync();
            roleMenus.ForEach(u =>
            {
                u.DeleteNow();
            });
            await _sysCacheService.SetAsync("IsChangeRoleMenu", true);
        }
    }
}
