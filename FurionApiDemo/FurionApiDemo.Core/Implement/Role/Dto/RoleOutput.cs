﻿using FurionApiDemo.Core.Implement.Menu.Dto;
using System.Collections.Generic;

namespace FurionApiDemo.Core.Implement
{
    /// <summary>
    /// 登录用户角色参数
    /// </summary>
    public class RoleOutput
    {
        /// <summary>
        /// Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 状态（1正常 2停用 3删除）
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 授权的菜单id
        /// </summary>
        public List<MenuOutput> MenuIds { get; set; }
    }
}
