﻿using FurionApiDemo.Core.Extension;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FurionApiDemo.Core.Implement
{
    public class RoleInput
    {
        /// <summary>
        /// 角色Id
        /// </summary>
        public virtual long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }
    /// <summary>
    /// 角色参数
    /// </summary>
    public class RolePageInput : XnInputBase
    {
        /// <summary>
        /// 角色Id
        /// </summary>
        public virtual long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }

    public class GrantRoleMenuInput : RolePageInput
    {
        /// <summary>
        /// 角色Id
        /// </summary>
        [Required(ErrorMessageResourceName = "角色Id不能为空")]
        public override long Id { get; set; }
    }

    public class GrantRoleDataInput : GrantRoleMenuInput
    {

    }

    public class SaveRoleInput
    {
        /// <summary>
        /// 角色Id
        /// </summary>
        public long? Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Required(ErrorMessageResourceName = "角色名称不能为空")]
        public string Name { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        [Required(ErrorMessageResourceName = "角色编码不能为空")]
        public string Code { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 状态-正常_0、停用_1、删除_2
        /// </summary>
        public CommonStatus Status { get; set; }
    }
}
