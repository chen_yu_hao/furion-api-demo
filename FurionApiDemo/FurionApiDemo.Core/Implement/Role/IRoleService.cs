﻿using FurionApiDemo.Core.ApiResult;
using FurionApiDemo.Core.Entity;
using FurionApiDemo.Core.Implement.Role.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FurionApiDemo.Core.Implement
{
    public interface IRoleService
    {
        Task DeleteRole(List<long> ids);
        Task<dynamic> GetRoleDropDown();
        Task<RoleOutput> GetRoleInfo(long id);
        Task<PagedList<RoleListOutput>> GetRoleList(RolePageInput input);
        Task GrantMenu(GrantRoleMenuInput input);
        Task SaveRole(SaveRoleInput input);
    }
}