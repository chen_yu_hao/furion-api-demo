﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurionApiDemo.Core.Implement.Auth.Dtos
{
    public class LoginOutput
    {
        /// <summary>
        /// Token令牌
        /// </summary>
        public string Token { get; set; }
        /// <summary>
        /// 刷新Token令牌
        /// </summary>
        public string RefreshToken { get; set; }
    }
}
