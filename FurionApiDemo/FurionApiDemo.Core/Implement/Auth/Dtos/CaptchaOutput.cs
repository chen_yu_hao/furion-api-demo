﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurionApiDemo.Core.Implement.Auth.Dtos
{
    public class CaptchaOutput
    {
        /// <summary>
        /// 验证码图片
        /// </summary>
        public string CaptchaBase64 { get; set; }
        /// <summary>
        /// 验证码结果
        /// </summary>
        public int CaptchaResult { get; set; }
    }
}
