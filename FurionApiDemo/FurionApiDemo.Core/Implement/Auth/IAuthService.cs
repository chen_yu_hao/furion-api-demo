﻿using FurionApiDemo.Core.ApiResult;
using FurionApiDemo.Core.Implement.Auth.Dtos;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace FurionApiDemo.Core.Implementt.Auth
{
    public interface IAuthService
    {
        Task<LoginOutput> LoginAsync([Required] LoginInput input);
        Task LogoutAsync();
        Task<CaptchaOutput> GetCaptcha();
        Task SetLoginInfo();
    }
}
