﻿using FurionApiDemo.Core.Implementt.User.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FurionApiDemo.Core.Implement
{
    public interface IUserRoleService
    {
        Task DeleteUserRoleListByRoleId(long roleId);
        Task DeleteUserRoleListByUserId(long userId);
        Task<List<long>> GetUserRoleIdList(long userId);
        Task GrantRole(long UserId, List<long> RoleIds);
    }
}