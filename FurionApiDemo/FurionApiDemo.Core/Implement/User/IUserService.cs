﻿using FurionApiDemo.Core.ApiResult;
using FurionApiDemo.Core.Entity;
using FurionApiDemo.Core.Implement.User.Dtos;
using FurionApiDemo.Core.Implementt.User.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurionApiDemo.Core
{
    public interface IUserService
    {
        /// <summary>
        /// 获取当前用户信息
        /// </summary>
        /// <returns></returns>
        Task<UserOutput> GetUser();
        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="id">用户ID</param>
        /// <returns></returns>
        Task<UserOutput> GetUser(long id);
        /// <summary>
        /// 查询用户列表信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<PagedList<UserListOutput>> GetUserList(UserListInput input);
        /// <summary>
        /// 保存用户信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task SaveUser(SaveUserInput input);
        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task DeleteUser(List<long> ids);
        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="oldPassword"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        Task UpdataPassword(string oldPassword, string newPassword);
        /// <summary>
        /// 重置密码
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task ResetPassword(long id);
    }
}
