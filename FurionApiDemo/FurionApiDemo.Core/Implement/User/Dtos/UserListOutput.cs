﻿using FurionApiDemo.Util.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurionApiDemo.Core.Implement.User.Dtos
{
    public class UserListOutput
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 账号
        /// </summary>
        public string UserName { get; set; }


        /// <summary>
        /// 状态-正常_1、停用_2、删除_3
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        public string NickName { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        public string AvatarUrl { get; set; }

        /// <summary>
        /// 生日
        /// </summary>
        public DateTimeOffset? Birthday { get; set; }

        /// <summary>
        /// 性别-男_1、女_2
        /// </summary>
        public int Sex { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public string SexText
        {
            get
            {
                switch (Sex)
                {
                    case (int)Gender.MALE:
                    case (int)Gender.FEMALE:
                    case (int)Gender.UNKNOWN:
                        return EnumHelper.GetDescription(Sex, typeof(Gender));
                    default:
                        return EnumHelper.GetDescription((int)Gender.UNKNOWN, typeof(Gender));
                }
            }
        }

        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 手机
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public string CreateTime { get; set; }
    }
}
