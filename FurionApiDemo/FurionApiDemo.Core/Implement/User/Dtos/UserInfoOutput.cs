﻿using FurionApiDemo.Util.Helper;
using System;

namespace FurionApiDemo.Core.Implementt.User.Dtos
{
    public class UserInfoOutput
    {
        /// <summary>
        /// 昵称
        /// </summary>
        public string NickName { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        public string AvatarUrl { get; set; }

        /// <summary>
        /// 生日
        /// </summary>
        public DateTimeOffset? Birthday { get; set; }

        /// <summary>
        /// 性别-男_1、女_2
        /// </summary>
        public int Sex { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public string SexText
        {
            get
            {
                switch (Sex)
                {
                    case (int)Gender.MALE:
                    case (int)Gender.FEMALE:
                    case (int)Gender.UNKNOWN:
                        return EnumHelper.GetDescription(Sex, typeof(Gender));
                    default:
                        return EnumHelper.GetDescription((int)Gender.UNKNOWN, typeof(Gender));
                }
            }
        }

        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 手机
        /// </summary>
        public string Phone { get; set; }
    }
}
