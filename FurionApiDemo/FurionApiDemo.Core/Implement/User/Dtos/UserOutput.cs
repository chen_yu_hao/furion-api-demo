﻿using FurionApiDemo.Core.Implement;
using System.Collections.Generic;

namespace FurionApiDemo.Core.Implementt.User.Dtos
{

    /// <summary>
    /// 用户参数
    /// </summary>
    public class UserOutput
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 账号
        /// </summary>
        public string UserName { get; set; }


        /// <summary>
        /// 状态-正常_0、停用_1、删除_2
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 用户信息
        /// </summary>
        public UserInfoOutput UserInfo { get; set; }

        /// <summary>
        /// 用户角色
        /// </summary>
        public List<RoleOutput> UserRole { get; set; }
    }
}
