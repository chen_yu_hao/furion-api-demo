﻿using Furion.DependencyInjection;
using FurionApiDemo.Core.Implement.System.Dto;
using FurionApiDemo.Util.Helper;

namespace FurionApiDemo.Core
{
    public class SystemService : ISystemService, ITransient
    {
        public SysNetOutput GetNetInfo()
        {
            var result = new SysNetOutput();
            result.Browser = NetHelper.Browser;
            result.IP = NetHelper.Ip;
            result.WanIp = NetHelper.GetWanIp();
            result.UserAgent = NetHelper.UserAgent;
            return result;
        }
        public ComputerOutput GetComputerInfo()
        {
            var result = new ComputerOutput();
            result.ComputerInfo = ComputerHelper.GetComputerInfo();
            result.CPURate = ComputerHelper.GetCPURate();
            result.OSVersion = NetHelper.GetOSVersion();
            result.RunTime = ComputerHelper.GetRunTime();
            return result;
        }

        public RunInfoOutput GetRunInfo()
        {
            var result = new RunInfoOutput();
            result.CPURate = ComputerHelper.GetCPURate();
            result.RunTime = ComputerHelper.GetRunTime();
            return result;
        }
    }
}