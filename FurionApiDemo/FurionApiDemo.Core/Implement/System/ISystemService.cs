﻿using FurionApiDemo.Core.Implement.System.Dto;

namespace FurionApiDemo.Core
{
    public interface ISystemService
    {
        SysNetOutput GetNetInfo();
        ComputerOutput GetComputerInfo();
        RunInfoOutput GetRunInfo();
    }
}