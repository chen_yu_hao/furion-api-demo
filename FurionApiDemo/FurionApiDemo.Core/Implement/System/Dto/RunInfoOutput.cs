﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurionApiDemo.Core.Implement.System.Dto
{
    public class RunInfoOutput
    {
        /// <summary>
        /// CPU利用率
        /// </summary>
        public string CPURate { get; set; }
        /// <summary>
        /// 运行时间
        /// </summary>
        public string RunTime { get; set; }
    }
}
