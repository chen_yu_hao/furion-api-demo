﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurionApiDemo.Core.Implement.System.Dto
{
    public class SysNetOutput
    {
        /// <summary>
        /// 本地IP
        /// </summary>
        public string IP { get; set; }
        /// <summary>
        /// 公网IP
        /// </summary>
        public string WanIp { get; set; }
        /// <summary>
        /// 使用的浏览器
        /// </summary>
        public string Browser { get; set; }
        /// <summary>
        /// 用户代理
        /// </summary>
        public string UserAgent { get; set; }
    }
}
