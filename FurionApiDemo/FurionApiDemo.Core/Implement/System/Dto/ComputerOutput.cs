﻿using FurionApiDemo.Util.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurionApiDemo.Core.Implement.System.Dto
{
    public class ComputerOutput
    {
        /// <summary>
        /// 系统
        /// </summary>
        public string OSVersion { get; set; }
        /// <summary>
        /// CPU利用率
        /// </summary>
        public string CPURate { get; set; }
        /// <summary>
        /// 运行时间
        /// </summary>
        public string RunTime { get; set; }
        /// <summary>
        /// 服务器详情
        /// </summary>
        public ComputerInfo ComputerInfo { get; set; }
    }
}
