﻿using FurionApiDemo.Util.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurionApiDemo.Core.Implement.html.Dtos
{
    public sealed class HtmlItemJson
    {
        public List<ItemsOutput> Items { get; set; } = new List<ItemsOutput>();

        public static HtmlItemJson Instance { get; private set; } = null;

        private HtmlItemJson() { }

        static HtmlItemJson()
        {
            string FilePath = Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "items.json");
            try
            {
                string fileStr = FileHelper.ReadFile(FilePath);
                if (fileStr == null)
                {
                    Instance = new HtmlItemJson();
                    Instance.Save();
                }
                else
                {
                    Instance = JsonHelper.ToObject<HtmlItemJson>(fileStr);
                }
            }
            catch (Exception)
            {
                Instance = new HtmlItemJson();
                Instance.Save();
            }
        }
        public void Save()
        {
            var dir = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
            var fileName = Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "items.json");
            FileHelper.CreateDirectory(dir);
            var jsonStr = JsonConvert.SerializeObject(Instance, Formatting.Indented);
            FileHelper.CreateFile(fileName, jsonStr);
        }
    }
}
