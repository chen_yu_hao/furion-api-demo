﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurionApiDemo.Core.Implement.html.Dtos
{
    public class AddItemInput
    {
        public string Url { get; set; }
        public string Label { get; set; }
    }
    public class UpdateItemInput
    {
        public string Id { get; set; }
        public string Url { get; set; }
        public string Label { get; set; }
    }
    public class DeleteItemInput
    {
        public string Id { get; set; }
    }
    public class ItemsOutput
    {
        public ItemsOutput()
        {
        }

        public ItemsOutput(string id, string url, string label)
        {
            Id = id;
            Url = url;
            Label = label;
        }

        public string Id { get; set; }
        public string Url { get; set; }
        public string Label { get; set; }
    }
    public class ItemOutput
    {
        public List<ItemsOutput> Items { get; set; }
    }
}
