﻿using Furion.DependencyInjection;
using FurionApiDemo.Core.Implement.html.Dtos;
using FurionApiDemo.Util.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurionApiDemo.Core.Implement.html
{
    public class HtmlService : IHtmlService, ITransient
    {
        public HtmlService()
        {
        }

        public async Task<List<ItemsOutput>> GetItemsAsync()
        {
            var result = HtmlItemJson.Instance?.Items;
            return result;
        }

        public async Task<bool> AddItemAsync(AddItemInput input)
        {
            try
            {
                HtmlItemJson.Instance.Items.Add(new ItemsOutput(Guid.NewGuid().ToString("N"), input.Url, input.Label));
                HtmlItemJson.Instance.Save();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> DeleteItemAsync(DeleteItemInput input)
        {
            try
            {
                var item = HtmlItemJson.Instance.Items.Find(t => t.Id == input.Id);
                HtmlItemJson.Instance.Items.Remove(item);
                HtmlItemJson.Instance.Save();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> UpdateItemAsync(UpdateItemInput input)
        {
            try
            {
                var item = HtmlItemJson.Instance.Items.Find(t => t.Id == input.Id);
                item.Url = input.Url;
                item.Label = input.Label;
                HtmlItemJson.Instance.Save();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
