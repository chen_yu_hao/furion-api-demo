﻿using FurionApiDemo.Core.Implement.html.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurionApiDemo.Core.Implement.html
{
    public interface IHtmlService
    {
        Task<List<ItemsOutput>> GetItemsAsync();
        Task<bool> AddItemAsync(AddItemInput input);
        Task<bool> DeleteItemAsync(DeleteItemInput input);
        Task<bool> UpdateItemAsync(UpdateItemInput input);
    }
}
