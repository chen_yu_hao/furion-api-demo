﻿using FurionApiDemo.Core.ApiResult;
using FurionApiDemo.Core.Implement.Menu.Dto;
using FurionApiDemo.Core.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurionApiDemo.Core.Implement.Menu
{
    public interface IMenuService
    {
        Task<List<string>> GetLoginPermissionList();
        Task<List<MenuOutput>> GetLoginMenus();
        Task<List<MenuOutput>> GetMenus(MenuInput input);
        Task<List<MenuOutput>> GetMenuList(MenuInput input);
        Task<List<RouterOutput>> GetLoginRoutes();
        Task<List<MenuOutput>> GetLoginMenuList();
        Task<bool> RefenceMenuCache();
        Task<SaveMenuOutput> SaveMenu(SaveMenuInput input);
        Task<SaveMenuOutput> GetMenuById(long id);
        Task DeleteMenu(long id);
    }
}
