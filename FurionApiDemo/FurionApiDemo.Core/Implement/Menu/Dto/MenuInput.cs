﻿using Furion.DatabaseAccessor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurionApiDemo.Core.Implement.Menu.Dto
{
    public class MenuInput
    {
        /// <summary>
        /// 状态（字典 1正常 2停用 3删除）
        /// </summary>
        public int? Status { get; set; }

        /// <summary>
        /// 软删除
        /// </summary>
        public bool? IsDeleted { get; set; }
    }
}
