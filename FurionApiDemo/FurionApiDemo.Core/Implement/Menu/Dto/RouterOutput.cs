﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurionApiDemo.Core.Implement.Menu.Dto
{
    public class RouterOutput
    {
        //{ path: '/demo-echarts', component: _import('demo/echarts'), name: 'demo-echarts', meta: { title: '图表', isTab: true }
        /// <summary>
        /// 路由地址
        /// </summary>
        public string path { get; set; }
        /// <summary>
        /// 组件地址
        /// </summary>
        public string component { get; set; }
        /// <summary>
        /// 路由名称
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 路由信息
        /// </summary>
        public Meta meta { get; set; }
    }   
    public class Meta
    {
        /// <summary>
        /// 路由标题
        /// </summary>
        public string title { get; set; }
        /// <summary>
        /// 是否Tab展示
        /// </summary>
        public bool isTab { get; set; }
    }
}
