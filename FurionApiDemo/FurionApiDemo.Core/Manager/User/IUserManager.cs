﻿using FurionApiDemo.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurionApiDemo.Core.Manager.User
{
    public interface IUserManager
    {
        string Account { get; }
        string Name { get; }
        bool SuperAdmin { get; }
        SysUser User { get; }
        long UserId { get; }

        Task<SysUser> CheckUserAsync(long userId, bool tracking = true);

        Task<SysUserInfo> CheckUserInfoAsync(long userId, bool tracking = true);
    }
}
