﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FurionApiDemo.Core.Entity
{
    /// <summary>
    /// 用户表
    /// </summary>
    [Table("User")]
    public class SysUser : DEntityBase, IEntityTypeBuilder<SysUser>
    {
        public SysUser()
        {
            Status = (int)CommonStatus.ENABLE;
        }

        /// <summary>
        /// 用户名
        /// </summary>
        [Comment("用户名")]
        [Required, MaxLength(20)]
        public string UserName { get; set; }

        /// <summary>
        /// 密码（采用MD5加密）
        /// </summary>
        [Comment("密码")]
        [Required, MaxLength(100)]
        public string Password { get; set; }

        /// <summary>
        /// 微信UnionId
        /// </summary>
        [Comment("微信UnionId")]
        [MaxLength(50)]
        public string WXUnionId { get; set; }

        /// <summary>
        /// 微信公众号OpenId
        /// </summary>
        [Comment("微信公众号OpenId")]
        [MaxLength(50)]
        public string WXWebOpenId { get; set; }

        /// <summary>
        /// 微信小程序OpenId
        /// </summary>
        [Comment("微信小程序OpenId")]
        [MaxLength(50)]
        public string WXAppOpenId { get; set; }

        /// <summary>
        /// 微信移动端OpenId
        /// </summary>
        [Comment("微信移动端OpenId")]
        [MaxLength(50)]
        public string WXMobileOpenId { get; set; }

        /// <summary>
        /// 最后登录IP
        /// </summary>
        [Comment("最后登录IP")]
        [MaxLength(30)]
        public string LastLoginIp { get; set; }

        /// <summary>
        /// 最后登录时间
        /// </summary>
        [Comment("最后登录时间")]
        public DateTimeOffset LastLoginTime { get; set; }

        /// <summary>
        /// 状态-正常_1、停用_2、删除_3
        /// </summary>
        [Comment("状态-正常_1、停用_2、删除_3")]
        public int Status { get; set; }

        /// <summary>
        /// 管理员类型-超级管理员_1、管理员_2、非管理员_3
        /// </summary>
        [Comment("管理员类型-超级管理员_1、管理员_2、非管理员_3")]
        public int AdminType { get; set; }

        /// <summary>
        /// 多对多中间表（用户-角色）
        /// </summary>
        public virtual ICollection<SysUserRole> SysUserRoles { get; set; }

        /// <summary>
        /// 用户信息
        /// </summary>
        public virtual SysUserInfo SysUserInfo { get; set; }

        /// <summary>
        /// 配置多对多关系
        /// </summary>
        /// <param name="entityBuilder"></param>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        public void Configure(EntityTypeBuilder<SysUser> entityBuilder, DbContext dbContext, Type dbContextLocator)
        {
            //角色对应
            entityBuilder.HasOne(p => p.SysUserInfo)
                .WithOne(p => p.SysUser)
                .HasForeignKey<SysUserInfo>(c => c.UserId)
                .OnDelete(DeleteBehavior.ClientSetNull);
        }
    }
}
