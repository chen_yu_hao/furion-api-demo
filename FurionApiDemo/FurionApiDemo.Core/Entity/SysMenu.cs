﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FurionApiDemo.Core.Entity
{
    /// <summary>
    /// 菜单表
    /// </summary>
    [Table("Menu")]
    public class SysMenu : DEntityBase, IEntityTypeBuilder<SysMenu>
    {
        public SysMenu()
        {
            Status = (int)CommonStatus.ENABLE;
        }

        /// <summary>
        /// 父Id
        /// </summary>
        public long Pid { get; set; }

        /// <summary>
        /// 父Ids
        /// </summary>
        public string Pids { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Required, MaxLength(32)]
        public string Name { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        [MaxLength(32)]
        public string Title { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 菜单类型（字典 0目录 1菜单 2按钮）
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 图标
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// 路由地址
        /// </summary>
        public string Router { get; set; }

        /// <summary>
        /// 组件地址
        /// </summary>
        public string Component { get; set; }

        /// <summary>
        /// 权限标识
        /// </summary>
        public string Permission { get; set; }

        /// <summary>
        /// 打开方式（字典 0无 1组件 2内链 3外链）
        /// </summary>
        public int OpenType { get; set; }

        /// <summary>
        /// 是否可见（Y-是，N-否）
        /// </summary>
        public string Visible { get; set; }

        /// <summary>
        /// 内链地址
        /// </summary>
        public string Link { get; set; }

        /// <summary>
        /// 重定向地址
        /// </summary>
        public string Redirect { get; set; }

        /// <summary>
        /// 权重（字典 1系统权重 2业务权重）
        /// </summary>
        public int Weight { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 是否tab显示（字典 0否 1是）
        /// </summary>
        public int IsTab { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 状态（字典 1正常 2停用 3删除）
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 多对多中间表（用户角色）
        /// </summary>
        public virtual ICollection<SysRoleMenu> SysRoleMenus { get; set; }

        /// <summary>
        /// 上级
        /// </summary>
        public virtual SysMenu Parent { get; set; }

        /// <summary>
        /// 子集
        /// </summary>
        public virtual ICollection<SysMenu> Childrens { get; set; }
        /// <summary>
        /// 配置实体关系
        /// </summary>
        /// <param name="entityBuilder"></param>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        public void Configure(EntityTypeBuilder<SysMenu> entityBuilder, DbContext dbContext, Type dbContextLocator)
        {
            entityBuilder.HasKey(t => new
            {
                t.Id
            });
            entityBuilder
                .HasMany(x => x.Childrens)
                .WithOne(x => x.Parent)
                .HasForeignKey(x => x.Pid)
                .OnDelete(DeleteBehavior.ClientSetNull); // 必须设置这一行
        }
    }
}
