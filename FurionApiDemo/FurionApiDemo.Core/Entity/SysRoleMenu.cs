﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FurionApiDemo.Core.Entity
{
    /// <summary>
    /// 角色菜单表
    /// </summary>
    [Table("RoleMenu")]
    public class SysRoleMenu : IEntity, IEntityTypeBuilder<SysRoleMenu>
    {
        /// <summary>
        /// 角色Id
        /// </summary>
        [Comment("角色Id")]
        [Required]
        public long RoleId { get; set; }

        /// <summary>
        /// 一对一引用（系统用户）
        /// </summary>
        public virtual SysRole SysRole { get; set; }

        /// <summary>
        /// 菜单Id
        /// </summary>
        [Comment("菜单Id")]
        [Required]
        public long MenuId { get; set; }

        /// <summary>
        /// 一对一引用（系统菜单）
        /// </summary>
        public virtual SysMenu SysMenu { get; set; }

        public void Configure(EntityTypeBuilder<SysRoleMenu> entityBuilder, DbContext dbContext, Type dbContextLocator)
        {
            entityBuilder.HasKey(t => new
            {
                t.RoleId,
                t.MenuId
            });
            entityBuilder.HasOne(t => t.SysRole)
                .WithMany(t => t.SysRoleMenus)
                .HasForeignKey(t => t.RoleId)
                .OnDelete(DeleteBehavior.ClientSetNull);
            entityBuilder.HasOne(t => t.SysMenu)
                .WithMany(t => t.SysRoleMenus)
                .HasForeignKey(t => t.MenuId)
                .OnDelete(DeleteBehavior.ClientSetNull);
        }
    }
}
