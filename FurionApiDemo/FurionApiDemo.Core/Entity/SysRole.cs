﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FurionApiDemo.Core.Entity
{
    /// <summary>
    /// 角色表
    /// </summary>
    [Table("Role")]
    public class SysRole : DEntityBase, IEntityTypeBuilder<SysRole>
    {
        public SysRole()
        {
            Status = (int)CommonStatus.ENABLE;
        }

        /// <summary>
        /// 名称
        /// </summary>
        [Comment("名称")]
        [Required, MaxLength(30)]
        public string Name { get; set; }

        /// <summary>
        /// 状态（1正常 2停用 3删除）
        /// </summary>
        [Comment("状态（1正常 2停用 3删除）")]
        public int Status { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        [Comment("编码")]
        [MaxLength(50)]
        public string Code { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        [Comment("排序")]
        [Required]
        public int Sort { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [Comment("备注")]
        public string Remark { get; set; }

        /// <summary>
        /// 多对多中间表（用户-角色）
        /// </summary>
        public virtual ICollection<SysUserRole> SysUserRoles { get; set; }

        /// <summary>
        /// 多对多中间表（角色-菜单）
        /// </summary>
        public virtual ICollection<SysRoleMenu> SysRoleMenus { get; set; }

        public void Configure(EntityTypeBuilder<SysRole> entityBuilder, DbContext dbContext, Type dbContextLocator)
        {
           
        }
    }
}
