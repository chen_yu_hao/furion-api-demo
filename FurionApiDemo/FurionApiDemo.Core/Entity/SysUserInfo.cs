﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurionApiDemo.Core.Entity
{
    /// <summary>
    /// 用户信息表
    /// </summary>
    [Table("UserInfo")]
    public class SysUserInfo : DEntityBase, IEntityTypeBuilder<SysUserInfo>
    {
        /// <summary>
        /// 用户名ID
        /// </summary>
        [Comment("用户名ID")]
        [Required]
        public long UserId { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        [Comment("昵称")]
        [Required, MaxLength(100)]
        public string NickName { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        [Comment("头像")]
        public string AvatarUrl { get; set; }

        /// <summary>
        /// 生日
        /// </summary>
        [Comment("生日")]
        public DateTimeOffset? Birthday { get; set; }

        /// <summary>
        /// 性别-男_1、女_2
        /// </summary>
        [Comment("性别-男_1、女_2")]
        public int Sex { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        [Comment("邮箱")]
        [MaxLength(30)]
        public string Email { get; set; }

        /// <summary>
        /// 手机
        /// </summary>
        [Comment("手机")]
        [MaxLength(30)]
        public string Phone { get; set; }

        /// <summary>
        /// 用户
        /// </summary>
        public SysUser SysUser { get; set; }

        public void Configure(EntityTypeBuilder<SysUserInfo> entityBuilder, DbContext dbContext, Type dbContextLocator)
        {

        }
    }
}
