﻿namespace FurionApiDemo.Core.Const
{
    public class CommonConst
    {
        /// <summary>
        /// 默认密码
        /// </summary>
        public const string DEFAULT_PASSWORD = "123456";

        /// <summary>
        /// 用户缓存
        /// </summary>
        public const string CACHE_KEY_USER = "_user";

        /// <summary>
        /// 菜单缓存
        /// </summary>
        public const string CACHE_KEY_MENU = "_menu";

        /// <summary>
        /// 权限缓存
        /// </summary>
        public const string CACHE_KEY_PERMISSION = "_permission";

        /// <summary>
        /// 角色缓存
        /// </summary>
        public const string CACHE_KEY_ROLE = "_role";

        /// <summary>
        /// 系统配置缓存
        /// </summary>
        public const string CACHE_KEY_CONFIG = "_config";

        /// <summary>
        /// 验证码缓存
        /// </summary>
        public const string CACHE_KEY_CODE = "_code";
    }
}
