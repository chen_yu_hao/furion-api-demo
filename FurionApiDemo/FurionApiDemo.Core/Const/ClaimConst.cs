﻿namespace FurionApiDemo.Core.Const
{
    public class ClaimConst
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public const string CLAINM_USERID = "Id";

        /// <summary>
        /// 账号
        /// </summary>
        public const string CLAINM_USER_NAME = "UserName";

        /// <summary>
        /// 名称
        /// </summary>
        public const string CLAINM_NAME = "NickName";

        /// <summary>
        /// 是否超级管理
        /// </summary>
        public const string CLAINM_SUPERADMIN = "SuperAdmin";

    }
}
