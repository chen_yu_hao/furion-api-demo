﻿using Furion.DatabaseAccessor;
using FurionApiDemo.Core.DbContextLocator;
using FurionApiDemo.Core.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace FurionApiDemo.Core
{
    /// <summary>
    /// 系统用户表种子数据
    /// </summary>
    public class SysUserSeedData : IEntitySeedData<SysUser>
    {
        /// <summary>
        /// 种子数据
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        /// <returns></returns>
        public IEnumerable<SysUser> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new[]
            {
                new SysUser {
                    Id=1,
                    UserName="superAdmin",
                    Password="e10adc3949ba59abbe56e057f20f883e",
                    WXUnionId="",
                    WXAppOpenId="",
                    WXMobileOpenId="",
                    WXWebOpenId="",
                    LastLoginIp="",
                    LastLoginTime=DateTime.Now,
                    AdminType=1,
                },
                new SysUser {
                    Id=2,
                    UserName="admin",
                    Password="e10adc3949ba59abbe56e057f20f883e",
                    WXUnionId="",
                    WXAppOpenId="",
                    WXMobileOpenId="",
                    WXWebOpenId="",
                    LastLoginIp="",
                    LastLoginTime=DateTime.Now,
                    AdminType=2,
                },
            };
        }
    }
}
