﻿using Furion.DatabaseAccessor;
using FurionApiDemo.Core.DbContextLocator;
using FurionApiDemo.Core.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace FurionApiDemo.Core.SeedData
{
    public class SysUserRoleSeedData : IEntitySeedData<SysUserRole>
    {
        /// <summary>
        /// 种子数据
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        /// <returns></returns>
        public IEnumerable<SysUserRole> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new[]
            {
                new SysUserRole { UserId = 1, RoleId = 1 },
                new SysUserRole { UserId = 2, RoleId = 2 }
            };
        }
    }
}
