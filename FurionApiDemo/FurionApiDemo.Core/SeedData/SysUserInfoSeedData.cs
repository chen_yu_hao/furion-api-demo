﻿using Furion.DatabaseAccessor;
using FurionApiDemo.Core.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurionApiDemo.Core.SeedData
{
    public class SysUserInfoSeedData : IEntitySeedData<SysUserInfo>
    {
        public IEnumerable<SysUserInfo> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new[]
            {
                new SysUserInfo
                {
                    Id=1,
                    UserId=1,
                    NickName="超级管理员",
                    AvatarUrl="",
                    Birthday=DateTimeOffset.Parse("2000-01-01 00:00:00"),
                    Sex=(int)Gender.MALE,
                    Email="379894344@qq.com",
                    Phone="13219041995"
                },
                new SysUserInfo
                {
                    Id=2,
                    UserId=2,
                    NickName="管理员",
                    AvatarUrl="",
                    Birthday=DateTimeOffset.Parse("2000-01-01 00:00:00"),
                    Sex=(int)Gender.MALE,
                    Email="379894344@qq.com",
                    Phone="13219041995"
                },
            };
        }
    }
}
