﻿using Furion.DatabaseAccessor;
using FurionApiDemo.Core.DbContextLocator;
using FurionApiDemo.Core.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace FurionApiDemo.Core
{
    /// <summary>
    /// 系统菜单表种子数据
    /// </summary>
    public class SysMenuSeedData : IEntitySeedData<SysMenu>
    {
        /// <summary>
        /// 种子数据
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        /// <returns></returns>
        public IEnumerable<SysMenu> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new[]
            {
                new SysMenu{Id=1, Pid=0, Pids="[0],", Name="首页", Title="首页", Code="system_home", Type=0, Icon="shouye", Router="/home", Component="common/home", OpenType=0, Visible="Y", Redirect="", Weight=1, Sort=1, IsTab=1, Status=1 },
                
                new SysMenu{Id=2, Pid=0, Pids="[0],", Name="主题", Title="主题", Code="system_theme", Type=0, Icon="theme", Router="/theme", Component="common/them", OpenType=0, Visible="Y", Redirect="", Weight=1, Sort=2, IsTab=1, Status=1 },
                
                new SysMenu{Id=3, Pid=0, Pids="[0],", Name="系统管理", Title="系统管理", Code="system_system_manager", Type=0, Icon="shezhi_xitong", Router="/system", Component="modules/sys", OpenType=0, Visible="Y", Redirect="", Weight=1, Sort=3, IsTab=1, Status=1 },
                
                new SysMenu{Id=4, Pid=3, Pids="[0],[3],", Name="系统信息", Title="系统信息", Code="system_manager_info", Type=1, Icon="shujuku", Router="/system/info", Component="modules/sys/info", OpenType=0, Visible="Y", Redirect="", Weight=1, Sort=1, IsTab=1, Status=1 },
                new SysMenu{Id=5, Pid=4, Pids="[0],[3],[4],", Name="系统信息查询", Title="", Code="system_manager_info_query", Type=0, Icon="", Router="", Component="", Permission="sysInfo:query", OpenType=0, Visible="Y", Redirect="", Weight=1, Sort=1, IsTab=1, Status=1 },
               
                new SysMenu{Id=6, Pid=3, Pids="[0],[3],", Name="菜单", Title="菜单", Code="system_manager_menu", Type=1, Icon="fenlei", Router="/system/menu", Component="modules/sys/menu", OpenType=0, Visible="Y", Redirect="", Weight=1, Sort=2, IsTab=1, Status=1 },
                new SysMenu{Id=7, Pid=6, Pids="[0],[3],[6],", Name="菜单查询", Title="", Code="system_manager_menu_query", Type=2, Icon="", Router="", Component="", Permission="sysMenu:query", OpenType=0, Visible="Y", Redirect="", Weight=1, Sort=1, IsTab=1, Status=1 },
                new SysMenu{Id=8, Pid=6, Pids="[0],[3],[6],", Name="菜单新增", Title="", Code="system_manager_menu_add", Type=2, Icon="", Router="", Component="", Permission="sysMenu:add", OpenType=0, Visible="Y", Redirect="", Weight=1, Sort=2, IsTab=1, Status=1 },
                new SysMenu{Id=9, Pid=6, Pids="[0],[3],[6],", Name="菜单编辑", Title="", Code="system_manager_menu_edit", Type=2, Icon="", Router="", Component="", Permission="sysMenu:edit", OpenType=0, Visible="Y", Redirect="", Weight=1, Sort=3, IsTab=1, Status=1 },
                new SysMenu{Id=10, Pid=6, Pids="[0],[3],[6],", Name="菜单删除", Title="", Code="system_manager_menu_delete", Type=2, Icon="", Router="", Component="", Permission="sysMenu:delete", OpenType=0, Visible="Y", Redirect="", Weight=1, Sort=4, IsTab=1, Status=1 },
                new SysMenu{Id=11, Pid=6, Pids="[0],[3],[6],", Name="菜单刷新缓存", Title="", Code="system_manager_menu_refence", Type=2, Icon="", Router="", Component="", Permission="sysMenu:refence", OpenType=0, Visible="Y", Redirect="", Weight=1, Sort=5, IsTab=1, Status=1 },

                new SysMenu{Id=12, Pid=3, Pids="[0],[3]", Name="用户", Title="用户", Code="system_manager_user", Type=1, Icon="kehu", Router="/system/user", Component="modules/sys/user", OpenType=0, Visible="Y", Redirect="", Weight=1, Sort=3, IsTab=1, Status=1 },
                new SysMenu{Id=13, Pid=12, Pids="[0],[3],[12],", Name="用户查询", Title="", Code="system_manager_user_query", Type=2, Icon="", Router="", Component="", Permission="sysUser:query", OpenType=0, Visible="Y", Redirect="", Weight=1, Sort=1, IsTab=1, Status=1 },
                new SysMenu{Id=14, Pid=12, Pids="[0],[3],[12],", Name="用户新增", Title="", Code="system_manager_user_add", Type=2, Icon="", Router="", Component="", Permission="sysUser:add", OpenType=0, Visible="Y", Redirect="", Weight=1, Sort=2, IsTab=1, Status=1 },
                new SysMenu{Id=15, Pid=12, Pids="[0],[3],[12],", Name="用户编辑", Title="", Code="system_manager_user_edit", Type=2, Icon="", Router="", Component="", Permission="sysUser:edit", OpenType=0, Visible="Y", Redirect="", Weight=1, Sort=3, IsTab=1, Status=1 },
                new SysMenu{Id=16, Pid=12, Pids="[0],[3],[12],", Name="用户删除", Title="", Code="system_manager_user_delete", Type=2, Icon="", Router="", Component="", Permission="sysUser:delete", OpenType=0, Visible="Y", Redirect="", Weight=1, Sort=4, IsTab=1, Status=1 },
                new SysMenu{Id=17, Pid=12, Pids="[0],[3],[12],", Name="用户详情", Title="", Code="system_manager_user_detail", Type=2, Icon="", Router="", Component="", Permission="sysUser:detail", OpenType=0, Visible="Y", Redirect="", Weight=1, Sort=5, IsTab=1, Status=1 },
                new SysMenu{Id=18, Pid=12, Pids="[0],[3],[12],", Name="用户授权角色", Title="", Code="system_manager_user_role", Type=2, Icon="", Router="", Component="", Permission="sysUser:userRole", OpenType=0, Visible="Y", Redirect="", Weight=1, Sort=6, IsTab=1, Status=1 },

                new SysMenu{Id=19, Pid=3, Pids="[0],[3],", Name="角色", Title="角色", Code="system_manager_role", Type=1, Icon="tuandui", Router="/system/role", Component="modules/sys/role", OpenType=0, Visible="Y", Redirect="", Weight=1, Sort=4, IsTab=1, Status=1 },
                new SysMenu{Id=20, Pid=19, Pids="[0],[3],[19],", Name="角色查询", Title="", Code="system_manager_role_query", Type=2, Icon="", Router="", Component="", Permission="sysRole:query", OpenType=0, Visible="Y", Redirect="", Weight=1, Sort=1, IsTab=1, Status=1 },
                new SysMenu{Id=21, Pid=19, Pids="[0],[3],[19],", Name="角色新增", Title="", Code="system_manager_role_add", Type=2, Icon="", Router="", Component="", Permission="sysRole:add", OpenType=0, Visible="Y", Redirect="", Weight=1, Sort=2, IsTab=1, Status=1 },
                new SysMenu{Id=22, Pid=19, Pids="[0],[3],[19],", Name="角色编辑", Title="", Code="system_manager_role_edit", Type=2, Icon="", Router="", Component="", Permission="sysRole:edit", OpenType=0, Visible="Y", Redirect="", Weight=1, Sort=3, IsTab=1, Status=1 },
                new SysMenu{Id=23, Pid=19, Pids="[0],[3],[19],", Name="角色删除", Title="", Code="system_manager_role_delete", Type=2, Icon="", Router="", Component="", Permission="sysRole:delete", OpenType=0, Visible="Y", Redirect="", Weight=1, Sort=4, IsTab=1, Status=1 },
                new SysMenu{Id=24, Pid=19, Pids="[0],[3],[19],", Name="角色详情", Title="", Code="system_manager_role_detail", Type=2, Icon="", Router="", Component="", Permission="sysRole:detail", OpenType=0, Visible="Y", Redirect="", Weight=1, Sort=5, IsTab=1, Status=1 },
                new SysMenu{Id=25, Pid=19, Pids="[0],[3],[19],", Name="角色授权", Title="", Code="system_manager_role_menu", Type=2, Icon="", Router="", Component="", Permission="sysRole:roleMenu", OpenType=0, Visible="Y", Redirect="", Weight=1, Sort=6, IsTab=1, Status=1 },

                new SysMenu{Id=26, Pid=3, Pids="[0],[3],", Name="系统配置", Title="系统配置", Code="system_manager_config", Type=1, Icon="xiuli", Router="/system/config", Component="modules/sys/config", OpenType=0, Visible="Y", Redirect="", Weight=1, Sort=5, IsTab=1, Status=1 },
                new SysMenu{Id=27, Pid=26, Pids="[0],[3],[26],", Name="系统配置查询", Title="", Code="system_manager_config_query", Type=2, Icon="", Router="", Component="", Permission="sysConfig:query", OpenType=0, Visible="Y", Redirect="", Weight=1, Sort=1, IsTab=1, Status=1 },
                new SysMenu{Id=28, Pid=26, Pids="[0],[3],[26],", Name="系统配置编辑", Title="", Code="system_manager_config_edit", Type=2, Icon="", Router="", Component="", Permission="sysConfig:edit", OpenType=0, Visible="Y", Redirect="", Weight=1, Sort=2, IsTab=1, Status=1 },

                new SysMenu{Id=29, Pid=3, Pids="[0],[3],", Name="系统日志", Title="系统日志", Code="system_manager_log", Type=1, Icon="wenjian", Router="/system/log", Component="modules/sys/log", OpenType=0, Visible="Y", Redirect="", Weight=1, Sort=6, IsTab=1, Status=1 },
                new SysMenu{Id=30, Pid=29, Pids="[0],[3],[29],", Name="系统日志查询", Title="", Code="system_manager_log_query", Type=2, Icon="", Router="", Component="", Permission="sysLog:query", OpenType=0, Visible="Y", Redirect="", Weight=1, Sort=1, IsTab=1, Status=1 },
                new SysMenu{Id=31, Pid=29, Pids="[0],[3],[29],", Name="系统日志清理", Title="", Code="system_manager_log_clear", Type=2, Icon="", Router="", Component="", Permission="sysLog:clear", OpenType=0, Visible="Y", Redirect="", Weight=1, Sort=2, IsTab=1, Status=1 },

                new SysMenu{Id=32, Pid=0, Pids="[0],", Name="开发管理", Code="system_manager_tools", Type=0, Icon="SCMliucheng", Router="/tools", Component="/PageView", OpenType=0, Visible="Y", Weight=1, Sort=100, Status=1 },
                new SysMenu{Id=33, Pid=32, Pids="[0],[32],", Name="接口文档", Code="system_manager_tools_swagger", Icon="jianbao", Type=1, Router="/swagger", Component="common/Iframe", OpenType=2, Visible="Y", Link="http://localhost:53785/", Weight=1, Sort=1, Status=1},
            };
        }
    }
}
