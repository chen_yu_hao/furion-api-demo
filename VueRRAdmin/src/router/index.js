/**
 * 全站路由配置
 *
 * 建议:
 * 1. 代码中路由统一使用name属性跳转(不使用path属性)
 */
import Vue from "vue";
import Router from "vue-router";
import http from "@/utils/httpRequest";
import { isURL } from "@/utils/validate";
import { clearLoginInfo } from "@/utils";
import { GetLoginMenusApi, LoginPermissionListApi } from "@/api/menu";

import { Message } from 'element-ui';
Vue.use(Router);

// 开发环境不使用懒加载, 因为懒加载页面太多的话会造成webpack热更新太慢, 所以只有生产环境使用懒加载
const _import = require("./import-" + process.env.NODE_ENV);

// 全局路由(无需嵌套上左右整体布局)
const globalRoutes = [
  {
    path: "/404",
    component: _import("common/404"),
    name: "404",
    meta: { title: "404未找到" }
  },
  {
    path: "/login",
    component: _import("common/login"),
    name: "login",
    meta: { title: "登录" }
  }
];

// 主入口路由(需嵌套上左右整体布局)
const mainRoutes = {
  path: "/",
  component: _import("main"),
  name: "main",
  redirect: { path: "home" },
  meta: { title: "主入口整体布局" },
  children: [
    // 通过meta对象设置路由展示方式
    // 1. isTab: 是否通过tab展示内容, true: 是, false: 否
    // 2. iframeUrl: 是否通过iframe嵌套展示内容, '以http[s]://开头': 是, '': 否
    // 提示: 如需要通过iframe嵌套展示内容, 但不通过tab打开, 请自行创建组件使用iframe处理!
    // { path: '/home', component: _import('common/home'), name: 'home', meta: { title: '首页' } },
    // { path: '/theme', component: _import('common/theme'), name: 'theme', meta: { title: '主题' } },
    // { path: '/demo-echarts', component: _import('demo/echarts'), name: 'demo-echarts', meta: { title: '图表', isTab: true } },
    // { path: '/demo-ueditor', component: _import('demo/ueditor'), name: 'demo-ueditor', meta: { title: '富文本', isTab: true } },
    // { path: '/system/user', component: _import('modules/sys/user'), name: 'system-user', meta: { title: '用户', isTab: true } },
    // { path: '/system/role', component: _import('modules/sys/role'), name: 'system-role', meta: { title: '角色', isTab: true } },
    // { path: '/system/menu', component: _import('modules/sys/menu'), name: 'system-menu', meta: { title: '菜单', isTab: true } },
    // { path: '/system/log', component: _import('modules/sys/log'), name: 'system-log', meta: { title: '系统日志', isTab: true } },
    // { path: '/system/config', component: _import('modules/sys/config'), name: 'system-config', meta: { title: '系统配置', isTab: true } },
    // { path: '/system/info', component: _import('modules/sys/info'), name: 'system-info', meta: { title: '系统信息', isTab: true } }
  ],
  beforeEnter(to, from, next) {
    let token = localStorage.token;
    if (!token || !/\S/.test(token)) {
      clearLoginInfo();
      next({ path: "login" });
    }
    next();
  }
};

const router = new Router({
  mode: "hash",
  scrollBehavior: () => ({ y: 0 }),
  isAddDynamicMenuRoutes: false, // 是否已经添加动态(菜单)路由
  routes: globalRoutes.concat(mainRoutes)
});

router.beforeEach((to, from, next) => {
  // 添加动态(菜单)路由
  // 1. 已经添加 or 全局路由, 直接访问
  // 2. 获取菜单列表, 添加并保存本地存储
  if (
    router.options.isAddDynamicMenuRoutes ||
    fnCurrentRouteType(to, globalRoutes) === "global"
  ) {
    next();
  } else {
    GetLoginMenusApi()
      .then(res => {
        if (res && res.data.code === 200) {
          if (res.data.data.length != 0) {
            fnAddDynamicMenuRoutes(res.data.data);
            router.options.isAddDynamicMenuRoutes = true;
            sessionStorage.setItem(
              "menuList",
              JSON.stringify(res.data.data || "[]")
            );
            next({ ...to, replace: true });
          } else {
            window.console.log(
              `当前用户没有权限，请联系管理员！！`,
              "color:blue"
            );
            Message.error("当前用户没有权限，请联系管理员！！");
            sessionStorage.removeItem("token");
            sessionStorage.removeItem("refreshToken");
            router.push({ path: "login" });
          }
        } else {
          sessionStorage.setItem("menuList", "[]");
          next();
        }
      })
      .catch(err => {
        window.console.log(
          `%c${err} 请求菜单列表失败，跳转至登录页！！`,
          "color:blue"
        );
        Message.error(`%c${err} 请求菜单列表失败，跳转至登录页！！`);
        router.push({ path: "login" });
      });
    LoginPermissionListApi()
      .then(res => {
        sessionStorage.setItem(
          "permissions",
          JSON.stringify(res.data.data || "[]")
        );
      })
      .catch(err => {
        window.console.log(`%c${err} 请求权限失败！！`, "color:blue");
      });
  }
});

/**
 * 判断当前路由类型, global: 全局路由, main: 主入口路由
 * @param {*} route 当前路由
 */
function fnCurrentRouteType(route, globalRoutes = []) {
  var temp = [];
  for (var i = 0; i < globalRoutes.length; i++) {
    if (route.path === globalRoutes[i].path) {
      return "global";
    } else if (
      globalRoutes[i].children &&
      globalRoutes[i].children.length >= 1
    ) {
      temp = temp.concat(globalRoutes[i].children);
    }
  }
  return temp.length >= 1 ? fnCurrentRouteType(route, temp) : "main";
}

/**
 * 添加动态(菜单)路由
 * @param {*} menuList 菜单列表
 * @param {*} routes 递归创建的动态(菜单)路由
 */
function fnAddDynamicMenuRoutes(menuList = [], routes = []) {
  var temp = [];
  for (var i = 0; i < menuList.length; i++) {
    if (menuList[i].childrens && menuList[i].childrens.length >= 1) {
      temp = temp.concat(menuList[i].childrens);
    } else if (menuList[i].router && /\S/.test(menuList[i].router)) {
      menuList[i].router = menuList[i].router.replace(/^\//, "");
      var route = {
        path: menuList[i].router,
        component: menuList[i].component,
        name: menuList[i].name,
        meta: {
          menuId: menuList[i].id,
          title: menuList[i].title,
          isDynamic: true,
          isTab: menuList[i].isTab,
          iframeUrl: menuList[i].link
        }
      };
      // url以http[s]://开头, 通过iframe展示
      // if (isURL(menuList[i].url)) {
      if (menuList[i].link) {
        route["component"] = _import("common/iframe");
        // route["path"] = `i-${menuList[i].router}`;
        route["name"] = `i-${menuList[i].name}`;
        route["meta"]["iframeUrl"] = menuList[i].link;
      } else {
        try {
          route["component"] = _import(`${menuList[i].component}`) || null;
        } catch (e) {}
      }
      routes.push(route);
    }
  }
  if (temp.length >= 1) {
    fnAddDynamicMenuRoutes(temp, routes);
  } else {
    try {
      mainRoutes.children = routes;
      router.addRoutes([mainRoutes, { path: "*", redirect: { name: "404" } }]);
    } catch (error) {
      window.console.log(error);
    }
    sessionStorage.setItem(
      "dynamicMenuRoutes",
      JSON.stringify(mainRoutes.children || "[]")
    );
    // console.log("\n");
    // console.log(
    //   "%c!<-------------------- 动态(菜单)路由 s -------------------->",
    //   "color:blue"
    // );
    // console.log(mainRoutes);
    // console.log(
    //   "%c!<-------------------- 动态(菜单)路由 e -------------------->",
    //   "color:blue"
    // );
  }
}

export default router;
