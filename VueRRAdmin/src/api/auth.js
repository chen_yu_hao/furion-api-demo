import api from "./index";
// axios
// 包装请求api . 使用： import { register } from '@/request/api';// 导入api接口
import { post, get } from "./http/request";
// 获取选择的区域
export const LoginApi = data => post(api.Login, data);
export const SetLoginInfoApi = () => post(api.SetLoginInfo);

export const GetCaptchaApi = () => post(api.GetCaptcha);
