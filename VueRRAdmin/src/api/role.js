import api from "./index";
// axios
// 包装请求api . 使用： import { register } from '@/request/api';// 导入api接口
import { post, get, deleteR, put } from "./http/request";

/**
 * 获取当前用户信息
 * @returns
 */
export const GetRoleListApi = data => get(api.GetRoleList, data);
/**
 * 获取角色下拉
 * @returns
 */
export const GetRoleDropDownApi = () => get(api.GetRoleDropDown);
/**
 * 保存角色
 * @returns
 */
export const SaveRoleApi = data => post(api.SaveRole, data);
/**
 * 删除角色
 * @param {角色id} id
 * @returns
 */
export const DeleteRoleApi = ids => deleteR(api.DeleteRole, ids);
/**
 * 获取角色信息
 * @param {*} id
 * @returns
 */
export const GetRoleInfoApi = id => get(api.GetRoleInfo, { id: id });
/**
 * 授权角色菜单
 * @param {*} data
 * @returns
 */
export const GrantMenuApi = data => put(api.GrantMenu, data);
