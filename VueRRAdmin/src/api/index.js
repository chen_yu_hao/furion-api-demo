const api = {
  Login: "auth/Login",
  SetLoginInfo: "/auth/SetLoginInfo",
  GetCaptcha: "auth/GetCaptcha",

  GetUser: "/user/GetUser",
  GetUserById: "/user/GetUserById",
  GetUserList: "/user/GetUserList",
  SaveUser: "/user/SaveUser",
  DeleteUser: "/user/DeleteUser",
  UpdataPassword: "/user/UpdataPassword",
  ResetPassword: "/user/ResetPassword",

  GetNetInfo: "/system/GetNetInfo",
  GetComputerInfo: "/system/GetComputerInfo",

  GetRoleList: "/Role/GetRoleList",
  GetRoleDropDown: "/Role/GetRoleDropDown",
  SaveRole: "/Role/SaveRole",
  DeleteRole: "/Role/DeleteRole",
  GetRoleInfo: "/Role/GetRoleInfo",
  GrantMenu: "/Role/GrantMenu",

  GetMenus: "/Menu/GetMenus",
  GetMenuList: "/Menu/GetMenuList",
  GetLoginMenus: "/Menu/GetLoginMenus",
  LoginPermissionList: "/Menu/LoginPermissionList",
  GetLoginRoutes: "/Menu/GetLoginRoutes",
  GetLoginMenuList: "/Menu/GetLoginMenuList",
  RefenceMenuCache: "/Menu/RefenceMenuCache",
  GetMenuById: "/Menu/GetMenuById",
  SaveMenu: "/Menu/SaveMenu",
  DeleteMenu: "/Menu/DeleteMenu"
};

export default api;
