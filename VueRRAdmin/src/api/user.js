import api from "./index";
// axios
// 包装请求api . 使用： import { register } from '@/request/api';// 导入api接口
import { post, get, deleteR, put } from "./http/request";

/**
 * 获取当前用户信息
 * @returns
 */
export const GetUserApi = () => get(api.GetUser);

/**
 * 获取用户详情
 * @param 用户ID data
 * @returns
 */
export const GetUserByIdApi = id => get(api.GetUserById + "?id=" + id);

/**
 * 获取用户列表
 * @param 用户ID data
 * @returns
 */
export const GetUserListApi = data => get(api.GetUserList, data);

/**
 * 保存用户信息
 * @param 用户ID data
 * @returns
 */
export const SaveUserApi = data => post(api.SaveUser, data);
/**
 * 删除用户
 * @returns
 */
export const DeleteUserApi = data => deleteR(api.DeleteUser, data);
/**
 * 更新密码
 * @param {原密码} oldPsw 
 * @param {新密码} newPsw 
 * @returns 
 */
export const UpdataPasswordApi = (oldPsw, newPsw) =>
  put(api.UpdataPassword + "?oldPassword=" + oldPsw + "&newPassword=" + newPsw);


/**
 * 重置密码
 * @param 用户ID data
 * @returns
 */
 export const ResetPasswordApi = id => put(api.ResetPassword + "?id=" + id);
  
