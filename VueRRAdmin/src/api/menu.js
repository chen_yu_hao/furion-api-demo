import api from "./index";
// axios
// 包装请求api . 使用： import { register } from '@/request/api';// 导入api接口
import { post, get } from "./http/request";

/**
 * 获取当前用户菜单
 * @returns
 */
export const GetLoginMenusApi = () => get(api.GetLoginMenus);
/**
 * 获取当前用户按钮权限
 * @returns
 */
export const LoginPermissionListApi = () => get(api.LoginPermissionList);
/**
 * 获取当前用户路由
 * @returns
 */
export const GetLoginRoutesApi = () => get(api.GetLoginRoutes);
/**
 * 获取菜单
 * @returns
 */
export const GetMenusApi = param => get(api.GetMenus, param);
/**
 * 获取菜单
 * @returns
 */
export const GetMenuListApi = param => get(api.GetMenuList, param);
/**
 * 获取当前用户菜单列表
 * @returns
 */
export const GetLoginMenuListApi = () => get(api.GetLoginMenuList);
/**
 * 刷新菜单缓存
 * @returns
 */
export const RefenceMenuCacheApi = () => post(api.RefenceMenuCache);
/**
 * 获取菜单详情
 * @returns
 */
export const GetMenuByIdApi = id => get(api.GetMenuById, { id: id });
/**
 * 保存菜单
 * @returns
 */
export const SaveMenuApi = param => post(api.SaveMenu, param);
/**
 * 删除菜单
 * @returns
 */
export const DeleteMenuApi = id => deleteR(api.DeleteMenu + "?id=" + id);
